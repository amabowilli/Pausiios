 //
//  AppDelegate.swift
//  Pausi
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 mac. All rights reserved.
//
 
 /*
  var lan = ""
  if item == "English" {
  L102Language.setAppleLAnguageTo(lang: "en")
  lan = "en"
  } else {
  L102Language.setAppleLAnguageTo(lang: "ar")
  lan = "ar"
  }
  */
 
 
 
let UT_USER = "USER"
let UT_AGENT = "AGENT"

let keyUserInfo = "userInfo"
let googleApiKey = "AIzaSyCo2eGa5b9mUSxWq9Ukr5hpe7TkhlA70GQ"

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import FBSDKCoreKit
import GoogleSignIn
import CoreLocation
import GooglePlacePicker
import UserNotifications
import Alamofire
import Firebase
import FirebaseMessaging
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    var token = "iOS123"
    var userInfo = NSMutableDictionary()
    var userId = ""
    var userType = ""
    var propertyImage: UIImage?
    var navController: UINavigationController? = nil
    var pushToken = "iOS123"
    var profileUrl = ""
    var currentLanguage = "en"
    var currentLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    var locationManager = CLLocationManager()
    var propertyChatID = ""
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)], for: .normal)
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = "638511997642-2721eqh9kdn231ug93ct0i2mm5crt8hj.apps.googleusercontent.com"
        IQKeyboardManager.shared.enable = true
        GMSPlacesClient.provideAPIKey(googleApiKey)
        GMSServices.provideAPIKey(googleApiKey)
        GADMobileAds.configure(withApplicationID: "ca-app-pub-9304311353435424~8452472767")
        registerForPushNotification()
        requestForLocation()
        FirebaseApp.configure()
        manageSession()
        L102Localizer.DoTheMagic()
        if InstanceID.instanceID().token() != nil {
            let refreshedToken = InstanceID.instanceID().token()!
            print("InstanceID token: \(refreshedToken)")
            pushToken = (refreshedToken as NSString) as String
            print("strNotificationToken: \(pushToken)")
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        //wsChangeStatus("Offline")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //wsChangeStatus("Online")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func manageSession() {
        if let data = UserDefaults.standard.data(forKey: keyUserInfo) {
            if let dict = NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary {
                print("userInfo-\(dict)-")
                userId = string(dict, "id")
                userType = string(dict, "user_type")
                userInfo = dict.mutableCopy() as! NSMutableDictionary
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                window?.rootViewController = vc
                wsChangeStatus("Online")
            }
        }
    }
    
    //MARK: LOGOUT
    func logoutUser() {
        self.userInfo.removeAllObjects()
        self.userId = ""
        self.userType = ""
        self.profileUrl = ""
        UserDefaults.standard.removeObject(forKey: keyUserInfo)
        UserDefaults.standard.synchronize()
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "NavLanding")
        self.window?.rootViewController = vc
        self.wsChangeStatus("Offline")
        User.logOutUser { (status) in
//            if status == true {
//                self.dismiss(animated: true, completion: nil)
//            }
        }
    }
    
    //MARK: FACEBOOK, GOOGLE
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        return handled
    }
    
    //MARK: Request For Location
    func requestForLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.last != nil {
            currentLocation = locations.last!
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //Http.alert(appName, error.localizedDescription)
    }
    
    func registerForPushNotification()  {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { (granted, error) in
                guard granted else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
            Messaging.messaging().delegate = self
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    //MARK: NOTIFICATION DELEGATE
    /*func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map{ data -> String in
            return String(format: "%02.2hhx", data)
        }
        token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "token")
        print("token-\(token)-")
        
        InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.sandbox)
        let refreshedToken = InstanceID.instanceID().token()
        
        if refreshedToken != nil {
            pushToken = refreshedToken!
            print("2Device Token: \(pushToken)")
        }
        
    }*/
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map{ data -> String in
            return String(format: "%02.2hhx", data)
        }
        token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "token")
        print("token-\(token)-")
        
       Messaging.messaging().apnsToken = deviceToken
       Auth.auth().apnsToken = deviceToken
       Messaging.messaging().setAPNSToken(deviceToken as Data, type: .prod)
        
        let refreshedToken = InstanceID.instanceID().token()
        
        if refreshedToken != nil {
            pushToken = refreshedToken!
            print("2Device Token: \(pushToken)")
        }
    }
    
        func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("error-\(error.localizedDescription)-")
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("remoteMessage.appData-\(remoteMessage.appData)-")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
    var obChat: ChatVC? = nil
    var obConversation: ConversationsVC? = nil

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("userInfo-\(userInfo)-")
        Messaging.messaging().appDidReceiveMessage(userInfo)
        var isSend = false
        if let dict = userInfo["aps"] as? NSDictionary {
            
            if let alert = dict.object(forKey: "alert") as? NSDictionary {
                if string(alert, "title") == "You have new chat message." {
                    isSend = true
                    if obChat == nil && obConversation == nil {
                        goToChatScreen(alert)
                    }
                }
            }  else if string(dict, "alert") == "Delete User successfully " {
                logoutUser()
            }
            
            if !isSend {
                goToNotificationScreen(dict)
            }
        }
    }
    
    
    
    @available(iOS 10.0, *)
    private func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        //Handle the notification
        completionHandler(
            [.alert,
             .sound,
             .badge])
    }
    
    func goToChatScreen(_ dict: NSDictionary) {
        let alert = UIAlertController(title: appName, message: string(dict, "title"), preferredStyle: .alert)
        
        let actionYes = UIAlertAction(title: "See".getLoclized(), style: .default, handler: { action in
            
            let state = UIApplication.shared.applicationState
            if state == .background  || state == .inactive{
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                vc.selectedIndex = 1
            }else if state == .active {
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                vc.selectedIndex = 1
                self.window?.rootViewController = vc
            }            
        })
        
        let actionCancel = UIAlertAction(title: "Later".getLoclized(), style: .destructive, handler: { action in
            print("action cancel handler")
        })
        
        alert.addAction(actionYes)
        alert.addAction(actionCancel)
        
        DispatchQueue.main.async {
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func goToNotificationScreen(_ dict: NSDictionary) {
        let alert = UIAlertController(title: appName, message: string(dict, "alert"), preferredStyle: .alert)
        
        let actionYes = UIAlertAction(title: "See".getLoclized(), style: .default, handler: { action in
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "NotificationList") as! NotificationList
            let navVc = UINavigationController(rootViewController: vc)
            navVc.isNavigationBarHidden = true
            self.window?.rootViewController = navVc
        })
        
        let actionCancel = UIAlertAction(title: "Later".getLoclized(), style: .destructive, handler: { action in
            print("action cancel handler")
        })
        
        alert.addAction(actionYes)
        alert.addAction(actionCancel)
        
        DispatchQueue.main.async {
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: WS_CHANGE_STATUS
    func wsChangeStatus(_ status: String) {
        
        if userId != "" {
            let params = NSMutableDictionary()
            params["user_id"] = userId
            params["status"] = status
            params["timezone"] = PredefinedConstants.localTimeZoneName
            params["last_seen"] = getCurrentDate("dd/MM/yyyy HH:mm:ss")
            
            let api = api_update_online_offline_status
            Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                print("url-\(api)-")
                print("params-\(params)-")
                if response.result.value != nil {
                    if let json = response.result.value as? NSDictionary {
                        print("responce-\(json)-")
                        if number(json, "status").boolValue {
                            
                        } else {
                            //Http.alert(appName, string(json, "message"))
                        }
                    }
                } else {
                    //Http.alert(appName, response.error?.localizedDescription)
                    print("response-\(String(describing: response.result.error?.localizedDescription))-")
                    print("response-\(response.result.description)-")
                    print("response-\(response.result.debugDescription)-")
                }
            }
        }
    }
    
}

