//
//  SignUp.swift
//  Pausi
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignUp: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    //MARK: VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionRealEstate(_ sender: Any) {
        kAppDelegate.userType = UT_AGENT
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllServices") as! AllServices
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    func signUp() {
        Auth.auth().createUser(withEmail: "kundan@gmail.com", password: "qwerty") { (user, error) in
            
            if error == nil {
                print("You have successfully signed up")
                //The email address is already in use by another account.
                
                
            } else {
                print("error?.localizedDescription-\(error?.localizedDescription)-")
            }
        }
    }
    
    @IBAction func actionNext(_ sender: Any) {
        kAppDelegate.userType = UT_USER
        if let strMsg = checkValidation() {
            Http.alert(appName, strMsg)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpFinal") as! SignUpFinal
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: FUNCTIONS
    func checkValidation() -> String? {
        if tfEmail.text?.count == 0 {
            return AlertMsg.blankEmail
        } else if !(tfEmail.text!.isEmail) {
            return AlertMsg.invalidEmail
        } else if tfPassword.text?.count == 0 {
            return AlertMsg.blankPassword
        } else if tfConfirmPassword.text?.count == 0 {
            return AlertMsg.blankConfPass
        } else if tfConfirmPassword.text! != tfPassword.text! {
            return AlertMsg.passMisMatch
        }
        return nil
    }
    
    
}//Class End

