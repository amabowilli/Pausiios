//
//  SignUpFinal.swift
//  Pausi
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import CountryList
import Alamofire
import Firebase

class SignUpFinal: UIViewController, CountryListDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var btnTerms: UIButton!
    
    //MARK: VARIABLES
    var countryList = CountryList()
    var isTermsAccept = false
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        getPrevClassValue()
        countryList.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
        _=self.navigationController?.popToViewController(vc, animated: true)
    }
    
    @IBAction func actionCountryCode(_ sender: Any) {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func actionAcceptTerms(_ sender: Any) {
        isTermsAccept = !isTermsAccept
        if isTermsAccept {
            btnTerms.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
        } else {
            btnTerms.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        if let strMsg = checkValidation() {
            Http.alert(appName, strMsg)
        } else {
            wsSignUp()
            //fireBaseSignup()
        }
    }
    
    //MARK: FUNCTIONS
    func checkValidation() -> String? {
        if tfFirstName.text?.count == 0 {
            return AlertMsg.blankFirstName
        } else if tfLastName.text?.count == 0 {
            return AlertMsg.blankLastName
        } else if tfNumber.text?.count == 0 {
            return AlertMsg.blankNumber
        } else if !(isTermsAccept) {
            return AlertMsg.acceptTerms
        }
        return nil
    }
    //MARK: COUNTRY LIST DELEGATE
    func selectedCountry(country: Country) {
        self.lblCountryCode.text = "+\(country.phoneExtension)"
        /*print(country.name)
        print(country.flag)
        print(country.countryCode)
        print(country.phoneExtension)*/
    }
    
    func fireBaseSignup() {
        User.registerUser(withName: "\(tfFirstName.text!) \(tfLastName.text!)", email: email, password: "qwerty") { [weak weakSelf = self] (status, error) in
                DispatchQueue.main.async {
                    if error == nil {
                        if status == true {
                            print("fireBaseSignup sucess")
                        }
                    } else {
                        print(error?.localizedDescription)
                        if error?.localizedDescription == "The email address is already in use by another account." {
                            self.fireBaseSocialLogin()
                        }
                    }
                }
        }
    }
    
    func fireBaseSocialLogin() {
        User.loginUser(withEmail: email, password: "qwerty") { [weak weakSelf = self](status, error) in
            DispatchQueue.main.async {
                print("error.login-\(error?.localizedDescription)-")
                if status == true {
                    print("fireBaseLogin Success")
                }
            }
        }
    }
    
    //MARK: WS_SIGN_UP
    func wsSignUp() {
        showHUD("Loading...".getLoclized())
        let params = [
            "first_name": tfFirstName.text!,
            "last_name": tfLastName.text!,
            "device_type": "Ios",
            "email": email,
            "password": password,
            "user_type": kAppDelegate.userType,
            "mobile": tfNumber.text!,
            "status": "Online",
            "country_code": lblCountryCode.text,
            "lat": "\(String(describing: kAppDelegate.currentLocation.coordinate.latitude))",
            "lon": "\(String(describing: kAppDelegate.currentLocation.coordinate.longitude))",
            "ios_device_token": kAppDelegate.token,
            "broker": "0"
            ] as [String : AnyObject]
        print("params-\(params)-")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            /*if self.imgProfilePicture != nil {
                multipartFormData.append(UIImageJPEGRepresentation(self.imgProfilePicture!, 0.5)!, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }*/
            
            for (key, value) in params {
                //multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: api_signup)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("progress-\(progress)-")
                })
                
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        self.hideHUD()
                        print("url-\(api_signup)-")
                        print("params-\(params)-")
                        if let json = response.result.value as? NSDictionary {
                            print("responce-\(json)-")
                            if number(json, "status").boolValue {
                                self.fireBaseSignup()
                                if let result = json.object(forKey: "result") as? NSDictionary {
                                    kAppDelegate.userId = string(result, "id")
                                    kAppDelegate.userType = string(result, "user_type")
                                    kAppDelegate.profileUrl = string(result, "image")
                                    kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                                    UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                                    kAppDelegate.window?.rootViewController = vc
                                    self.navigationController?.viewControllers = []
                                }
                            } else {
                                Http.alert(appName, string(json, "message"))
                            }
                            
                        } else {
                            Http.alert(appName, response.error?.localizedDescription)
                        }
                    }
                }
            case .failure(let encodingError):
                //Http.alert(appName, encodingError.localizedDescription)
                print("responce-\(encodingError.localizedDescription)-")
            }
        }
    }
    
    var email = ""
    var password = ""
    
    func getPrevClassValue() {
        if let arr = self.navigationController?.viewControllers {
            for vc in arr {
                if vc is SignUp {
                    let vcc = vc as! SignUp
                    self.email = vcc.tfEmail.text!
                    self.password = vcc.tfPassword.text!
                }
            }
        }
    }
    
}//Class End
