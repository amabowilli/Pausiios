//
//  UsersListVC.swift
//  Pausi
//
//  Created by mac on 27/03/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class UsersListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: VARIABLES
    var arrList = NSMutableArray()
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.tableFooterView = UIView()
        wsGetUsers()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tfEditingChanged(_ sender: Any) {
    }
    
   
    //MARK: FUNCTIONS
    
    
    //MARK: UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell", for: indexPath) as! UserListCell
        let dict = self.arrList.object(at: indexPath.row) as! NSDictionary
        cell.imgView.sd_setImage(with: URL(string: string(dict, "image")), placeholderImage: UIImage(named: "default_profile.png"), options: SDWebImageOptions(rawValue: 1), completed: nil)
        cell.lblName.text = "\(string(dict, "first_name")) \(string(dict, "last_name"))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrList.object(at: indexPath.row) as! NSDictionary
        NotificationCenter.default.post(name: NSNotification.Name("UserDetail"), object: dict, userInfo: nil)
        _=self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: WS_GET_USERS
    func wsGetUsers() {
        let params = NSMutableDictionary()
        
        showHUD("Loading...".getLoclized())
        let api = api_get_all_user_list
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrList.removeAllObjects()
                            for i in 0..<result.count {
                                let dict = result.object(at: i) as! NSDictionary
                                if string(dict, "id") != kAppDelegate.userId {
                                    self.arrList.add(dict)
                                }
                            }
                            self.tblView.reloadData()
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    
}//Class End
