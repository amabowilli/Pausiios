//
//  MyProperties.swift
//  Pausi
//
//  Created by mac on 18/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class MyProperties: UIViewController, UITableViewDelegate, UITableViewDataSource, NewsCellDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var vwSoldPrice: UIView!
    @IBOutlet weak var tfSoldPrice: UITextField!
    
    //MARK: VARIABLES
    var arrNews = NSMutableArray()
    var buyerId = ""
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(showPopUp(notification:)), name: NSNotification.Name("UserDetail"), object: nil)
        vwSoldPrice.frame = self.view.frame
        vwSoldPrice.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }
    
    @objc func showPopUp(notification: NSNotification) {
        if let dict = notification.object as? NSDictionary {
            kAppDelegate.window?.addSubview(vwSoldPrice)
            buyerId = string(dict, "id")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        wsMyProperties()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCamera(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionRemoveFinalPrice(_ sender: Any) {
        
    }
    
    @IBAction func actionSubmitSoldPrice(_ sender: Any) {
        vwSoldPrice.removeFromSuperview()
        if tfSoldPrice.text?.count == 0 {
            Http.alert(appName, AlertMsg.blankSoldPrice)
        } else {
            wsChangeStatus(currentIndex)
        }
    }
    
    func actionMessage(index: IndexPath) {
        let dict = arrNews.object(at: index.row) as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateProperty") as! UpdateProperty
        vc.propertyId = string(dict, "id")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func actionFav(index: IndexPath) {
        //wsAddToFavourite(index)
    }
    
    var currentIndex = IndexPath()
    
    func acitonShare(index: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UsersListVC") as! UsersListVC
        self.navigationController?.pushViewController(vc, animated: true)
        currentIndex = index
        //wsChangeStatus(index)
    }
    
    func actionDetail(index: IndexPath) {
        let dict = arrNews.object(at: index.row) as! NSDictionary
        //let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dict, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: FUNCTIONS
    
    
    //MARK: UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.delegate = self
        cell.index = indexPath
        let dict = self.arrNews.object(at: indexPath.row) as! NSDictionary
        
        //let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
        if let arrImages = dict.object(forKey: "property_images") as? NSArray {
            cell.arrImages = arrImages
            cell.dictDetail = dict
            cell.dict = dict
            cell.cvSlider.reloadData()
        }
        
        /*if string(dict, "sale_type") == "Rent" {
            cell.lblSold.text = "Rented"
        } else {
            cell.lblFavorite.text = "Sold"
        }*/
        
        UIView.animate(withDuration: 0.5, animations: {
            //cell.cvSlider.frame.origin  = CGPoint(x : 22 , y: 3)
            cell.cvSlider.frame.origin  = CGPoint(x : 0 , y: 0)
        })
        
        cell.isCellOpen = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (PredefinedConstants.ScreenWidth*9)/16
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrNews.object(at: indexPath.row) as! NSDictionary
        //let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dict, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        /*
         if cell.isCellOpen {
         } else {
         self.tblbanklist.reloadData()
         }
         */
    }
    
    //MARK: WS_MY_PROPERTIES
    func wsMyProperties() {

        let params = NSMutableDictionary()
        params["agent_id"] = kAppDelegate.userId
        
        showHUD("Loading...".getLoclized())
        let api = api_my_listing_list
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrNews = result.mutableCopy() as! NSMutableArray
                            self.tblView.reloadData()
                            self.tblView.contentOffset.y = 0
                        }
                    } else {
                        if string(json, "message") == "Data Not Found" {
                            Http.alert(appName, "You have not added property.".getLoclized())
                        } else {
                            Http.alert(appName, string(json, "message"))
                        }
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    
    //MARK: WS_DELETE_PROPERTY
    func wsDeleteProperty(_ index: IndexPath) {

        let dict = arrNews.object(at: index.row) as! NSDictionary
        
        let params = NSMutableDictionary()
        params["property_id"] = string(dict, "id")
        
        showHUD("Loading...".getLoclized())
        let api = api_delete_property
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        self.arrNews.removeObject(at: index.row)
                        self.tblView.reloadData()
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    
    //MARK: WS_CHANG_STATUS
    func wsChangeStatus(_ index: IndexPath) {
        
        let dict = arrNews.object(at: index.row) as! NSDictionary
        
        let params = NSMutableDictionary()
        params["property_id"] = string(dict, "id")
        params["news_type"] = (string(dict, "sale_type") == "Sale" ) ? "Property Sold" : "Propperty Rented"
        params["status"] = (string(dict, "sale_type") == "Sale" ) ? "SOLD" : "NOT AVAILABLE"
        params["user_id"] = kAppDelegate.userId
        params["assign_user_id"] = buyerId
        params["sold_price"] = tfSoldPrice.text!
        
        showHUD("Loading...".getLoclized())
        let api = api_update_property_status
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        //self.arrNews.removeObject(at: index.row)
                        //self.tblView.reloadData()
                        self.wsMyProperties()
                        Http.alert(appName, AlertMsg.updateSuccess)
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    
}//Class End
