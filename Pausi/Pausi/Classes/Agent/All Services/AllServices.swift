//
//  AllServices.swift
//  Pausi
//
//  Created by mac on 19/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire

class AllServices: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: OUTLETS
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: VARIABLES
    var arrServices = NSArray()
    var isUpdate = false
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        wsGetAllServices()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: FUNCTIONS
    
    //MARK: UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllServicesCell", for: indexPath) as! AllServicesCell
        let dict = arrServices.object(at: indexPath.row) as! NSDictionary
        cell.lblShortName.text = string(dict, "sort_name")
        cell.lblFullName.text = string(dict, "service_name")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isUpdate {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AgentUpdateInfo") as! AgentUpdateInfo
            vc.isUpdate = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AgentSignUp") as! AgentSignUp
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: WS_GET_ALL_SERVICES
    func wsGetAllServices() {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        
        showHUD("Loading...".getLoclized())
        let api = api_all_services
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrServices = result.mutableCopy() as! NSMutableArray
                            self.tblView.reloadData()
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
}//Class End
