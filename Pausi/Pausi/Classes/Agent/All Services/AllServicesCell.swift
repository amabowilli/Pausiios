//
//  AllServicesCell.swift
//  Pausi
//
//  Created by mac on 19/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class AllServicesCell: UITableViewCell {

    @IBOutlet weak var lblShortName: UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
