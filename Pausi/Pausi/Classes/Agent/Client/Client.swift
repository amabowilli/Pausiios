//
//  Client.swift
//  Pausi
//
//  Created by mac on 11/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class Client: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    
    //MARK: VARIABLES
    
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func actionSegment(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        switch sender.selectedSegmentIndex {
        case 0:
            print("0")
        case 1:
            print("1")
        case 2:
            print("2")
        default:
            print("noon")
        }
    }
    
    
}//Class End
