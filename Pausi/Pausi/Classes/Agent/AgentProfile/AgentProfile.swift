//
//  AgentProfile.swift
//  Pausi
//
//  Created by mac on 19/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class AgentProfile: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBrokerType: UILabel!
    @IBOutlet var vwPopUp: UIView!
    
    //MARK: VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        vwPopUp.frame = self.view.frame
        vwPopUp.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        kAppDelegate.navController = self.navigationController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        wsGetProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionSetting(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AgentProfileDetail") as! AgentProfileDetail
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func actionCamera(_ sender: Any) {
    }
    
    @IBAction func actionProfile(_ sender: Any) {
        openFileAttachment()
    }
    
    @IBAction func actionInvite(_ sender: Any) {
        let activityController = UIActivityViewController(activityItems: [inviteText], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func actionFriends(_ sender: Any) {
        /*let vc = self.storyboard?.instantiateViewController(withIdentifier: "Client") as! Client
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
    
    @IBAction func actionMore(_ sender: Any) {
        //self.view.addSubview(vwPopUp)
        showMoreOption()
    }
    
    @IBAction func actionRemovePopUp(_ sender: Any) {
        vwPopUp.removeFromSuperview()
    }
    
    @IBAction func actionFavourite(_ sender: Any) {
        vwPopUp.removeFromSuperview()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyFavourites") as! MyFavourites
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionChangeProfileImage(_ sender: Any) {
        vwPopUp.removeFromSuperview()
        openFileAttachment()
    }
    
    @IBAction func actionChangeWallpaper(_ sender: Any) {
        vwPopUp.removeFromSuperview()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChooseWallpaper") as! ChooseWallpaper
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionEditProfile(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditAgentProfile") as! EditAgentProfile
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: FUNCTIONS
    func showMoreOption() {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "My Properties".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyProperties") as! MyProperties
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        actionSheet.addAction(UIAlertAction(title: "View Favourites".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyFavourites") as! MyFavourites
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        actionSheet.addAction(UIAlertAction(title: "Change Profile Image".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.openFileAttachment()
        }))
        actionSheet.addAction(UIAlertAction(title: "Change Wallpaper".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChooseWallpaper") as! ChooseWallpaper
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".getLoclized(), style: UIAlertActionStyle.cancel, handler: nil))
        if PredefinedConstants.deviceType == .pad {
            actionSheet.popoverPresentationController?.sourceView = self.view
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openFileAttachment() {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".getLoclized(), style: UIAlertActionStyle.cancel, handler: nil))
        if PredefinedConstants.deviceType == .pad {
            actionSheet.popoverPresentationController?.sourceView = self.view
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK: PICKERVIEW DELEGATE
    var imgProfilePicture:UIImage? = nil
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var img:UIImage? = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let iii = info[UIImagePickerControllerEditedImage] as? UIImage {
            img = iii
        }
        if (img != nil) {
            imgProfilePicture = img
            imgProfile.image = img
            wsUpdateProfile()
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: WS_UPDATE_PROFILE
    func wsUpdateProfile() {
        
        showHUD("Loading...".getLoclized())
        let params = [
            "user_id": kAppDelegate.userId,
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if self.imgProfilePicture != nil {
                multipartFormData.append(UIImageJPEGRepresentation(self.imgProfilePicture!, 0.5)!, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: api_update_user_image)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("progress-\(progress)-")
                })
                
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        self.hideHUD()
                        print("url-\(api_update_user_image)-")
                        print("params-\(params)-")
                        
                        if let json = response.result.value as? NSDictionary {
                            print("responce-\(json)-")
                            
                            if number(json, "status").boolValue {
                                if let result = json.object(forKey: "result") as? NSDictionary {
                                    kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                                    UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                                    Http.alert(appName, AlertMsg.updateSuccess)
                                }
                            } else {
                                Http.alert(appName, string(json, "result"))
                            }
                            
                        } else {
                            Http.alert(appName, response.error?.localizedDescription)
                        }
                    }
                }
            case .failure(let encodingError):
                //Http.alert(appName, encodingError.localizedDescription)
                print("responce-\(encodingError.localizedDescription)-")
            }
        }
    }
    
    //MARK: WS_GET_PROFILE
    func wsGetProfile() {
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        
        showHUD("Loading...".getLoclized())
        let api = api_get_profile
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                            UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                            kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                            self.imgProfile.sd_setImage(with: URL(string: string(result, "image")), placeholderImage: #imageLiteral(resourceName: "profile"), options: SDWebImageOptions(rawValue: 1), completed: nil)
                            self.imgBg.sd_setImage(with: URL(string: string(result, "wallpaper_image")), placeholderImage: #imageLiteral(resourceName: "bg_img"), options: SDWebImageOptions(rawValue: 1), completed: nil)
                            self.lblName.text = "\(string(result, "first_name")) \(string(result, "last_name"))"
                            self.lblBrokerType.text = string(result, "broker")
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    
}//Class End
