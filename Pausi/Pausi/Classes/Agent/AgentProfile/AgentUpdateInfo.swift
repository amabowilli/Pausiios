//
//  AgentUpdateInfo.swift
//  Pausi
//
//  Created by mac on 19/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class AgentUpdateInfo: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfBroker: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var imgProfile: ImageView!
    @IBOutlet weak var imgUpload: UIImageView!
    
    //MARK: VARIABLES
    var isProfile = true
    var isUpdate = false
    var isAddedId = true
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        showDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionProfile(_ sender: Any) {
        isProfile = true
        openFileAttachment()
    }
    
    @IBAction func actionAttachId(_ sender: Any) {
        isProfile = false
        openFileAttachment()
    }
    
    @IBAction func actionUpdate(_ sender: Any) {
        if let strMsg = checkValidation() {
            Http.alert(appName, strMsg)
        } else {
            wsUpdateProfile()
        }
    }
    
    @IBAction func actionUpdatePassword(_ sender: Any) {
        if let strMsg = validationPassword() {
            Http.alert(appName, strMsg)
        } else {
            wsChangePassword()
        }
    }
    
    @IBAction func actionPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/privacy_policy.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func actionTerms(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/terms_service.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    //MARK: FUNCTIONS
    func showDetails() {
        tfFirstName.text = string(kAppDelegate.userInfo, "first_name")
        tfLastName.text = string(kAppDelegate.userInfo, "last_name")
        tfBroker.text = string(kAppDelegate.userInfo, "broker")
        tfEmail.text = string(kAppDelegate.userInfo, "email")
        tfNumber.text = string(kAppDelegate.userInfo, "mobile")
        if string(kAppDelegate.userInfo, "id_proof").range(of: ".png") == nil {
            isAddedId = false
        }
        self.imgProfile.sd_setImage(with: URL(string: string(kAppDelegate.userInfo, "image")), placeholderImage: #imageLiteral(resourceName: "profile"), options: SDWebImageOptions(rawValue: 1), completed: nil)
        self.imgUpload.sd_setImage(with: URL(string: string(kAppDelegate.userInfo, "id_proof")), placeholderImage: #imageLiteral(resourceName: "upload.png"), options: SDWebImageOptions(rawValue: 1), completed: nil)
    }
    
    func checkValidation() -> String? {
        if tfFirstName.text?.count == 0 {
            return AlertMsg.blankFirstName
        } else if tfLastName.text?.count == 0 {
            return AlertMsg.blankLastName
        } else if !(isAddedId) {
            return AlertMsg.selectID
        } else if tfBroker.text?.count == 0 {
            return AlertMsg.blankBroker
        } else if tfEmail.text?.count == 0 {
            return AlertMsg.blankEmail
        } else if !(tfEmail.text!.isEmail) {
            return AlertMsg.invalidEmail
        } else if tfNumber.text?.count == 0 {
            return AlertMsg.blankNumber
        }
        return nil
    }
    
    func validationPassword() -> String? {
        if tfPassword.text?.count == 0 {
            return AlertMsg.blankCrntPass
        } else if tfNewPassword.text?.count == 0 {
            return AlertMsg.blankNewPass
        } else if tfConfirmPassword.text?.count == 0 {
            return AlertMsg.blankConfPass
        } else if tfConfirmPassword.text! != tfNewPassword.text! {
            return AlertMsg.passMisMatchNew
        }
        return nil
    }
    
    func openFileAttachment() {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".getLoclized(), style: UIAlertActionStyle.cancel, handler: nil))
        if PredefinedConstants.deviceType == .pad {
            actionSheet.popoverPresentationController?.sourceView = self.view
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK: PICKERVIEW DELEGATE
    var imgProfilePicture:UIImage? = nil
    var imgIdProof:UIImage? = nil

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var img:UIImage? = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let iii = info[UIImagePickerControllerEditedImage] as? UIImage {
            img = iii
        }
        if (img != nil) {
            if isProfile {
                imgProfilePicture = img
                imgProfile.image = img
            } else {
                isAddedId = true
                imgIdProof = img
                imgUpload.image = img
            }
            
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: WS_UPDATE_PROFILE
    func wsUpdateProfile() {
        
        let params = [
            "first_name": tfFirstName.text!,
            "last_name": tfLastName.text!,
            "broker": tfBroker.text!,
            "email": tfEmail.text!,
            "mobile": tfNumber.text!,
            "password": tfPassword.text!,
            "user_type": UT_AGENT,
            "user_id": kAppDelegate.userId,
            "country_code": "",
            "lat": "\(String(describing: kAppDelegate.currentLocation.coordinate.latitude))",
            "lon": "\(String(describing: kAppDelegate.currentLocation.coordinate.longitude))",
            "ios_register_id": kAppDelegate.token,
            "broker_title": string(kAppDelegate.userInfo, "broker_title"),
            "offer": string(kAppDelegate.userInfo, "offer"),
            "close_deals": string(kAppDelegate.userInfo, "close_deals"),
            "agent_since": string(kAppDelegate.userInfo, "agent_since")
            ] as [String : AnyObject]
        
        showHUD("Loading...".getLoclized())
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if self.imgProfilePicture != nil {
                multipartFormData.append(UIImageJPEGRepresentation(self.imgProfilePicture!, 0.5)!, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            
            if self.imgIdProof != nil {
                multipartFormData.append(UIImageJPEGRepresentation(self.imgIdProof!, 0.5)!, withName: "id_proof", fileName: "id_proof.jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: api_user_update)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("progress-\(progress)-")
                })
                
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        self.hideHUD()
                        print("url-\(api_user_update)-")
                        print("params-\(params)-")
                        if let json = response.result.value as? NSDictionary {
                            print("responce-\(json)-")
                            
                            if number(json, "status").boolValue {
                                if let result = json.object(forKey: "result") as? NSDictionary {
                                    kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                                    kAppDelegate.userType = UT_AGENT
                                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                                    UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                                    Http.alert(appName, AlertMsg.updateSuccess)
                                    if self.isUpdate {
                                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                                            _=self.navigationController?.popToRootViewController(animated: true)
                                            /*let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                                            kAppDelegate.window?.rootViewController = vc
                                            self.navigationController?.viewControllers = []*/
                                        })
                                    } else {
                                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                                            _=self.navigationController?.popViewController(animated: true)
                                        })
                                    }
                                }
                            } else {
                                Http.alert(appName, string(json, "message"))
                            }
                            
                        } else {
                            Http.alert(appName, response.error?.localizedDescription)
                        }
                    }
                }
            case .failure(let encodingError):
                //Http.alert(appName, encodingError.localizedDescription)
                print("responce-\(encodingError.localizedDescription)-")
            }
        }
    }
    
    //MARK: WS_CHANGE_PASSWORD
    func wsChangePassword() {
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["old_password"] = tfPassword.text!
        params["password"] = tfNewPassword.text!
        
        showHUD("Loading...".getLoclized())
        let api = api_change_password
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        Http.alert(appName, AlertMsg.updateSuccess)
                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                            _=self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    
}//Class End
