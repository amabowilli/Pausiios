//
//  EditAgentProfile.swift
//  Pausi
//
//  Created by mac on 31/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import DropDown
import Alamofire

class EditAgentProfile: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfBroker: UITextField!
    @IBOutlet weak var tfTagLine: UITextField!
    @IBOutlet weak var tfCloseDeals: UITextField!
    @IBOutlet weak var tfAgentSince: UITextField!
    
    //MARK: VARIABLES
    var dropDown = DropDown()
    var arrDeals = ["25+ Deals", "30+ Deals", "35+ Deals", "40+ Deals", "45+ Deals", "50+ Deals", "60+ Deals", "70+ Deals", "80+ Deals", "90+ Deals", "100+ Deals", "120+ Deals", "140+ Deals", "160+ Deals", "180+ Deals", "200+ Deals", "250+ Deals", "300+ Deals", "350+ Deals", "400+ Deals", "450+ Deals", "500+ Deals", "600+ Deals", "700+ Deals", "800+ Deals", "900+ Deals", "1000+ Deals" ]
    var arrYears = [String]()
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        for i in (1950...year).reversed() {
            arrYears.append("Agent Since \(i)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tfBroker.text = string(kAppDelegate.userInfo, "broker")
        tfTagLine.text = string(kAppDelegate.userInfo, "offer")
        tfCloseDeals.text = string(kAppDelegate.userInfo, "close_deals")
        tfAgentSince.text = string(kAppDelegate.userInfo, "agent_since")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCloseDeals(_ sender: Any) {
        createDropDown(arrDeals, tfCloseDeals)
    }
    
    @IBAction func actionAgentSince(_ sender: Any) {
        createDropDown(arrYears, tfAgentSince)
    }
    
    @IBAction func actionSave(_ sender: Any) {
        wsUpdateProfile()
    }
    //MARK: FUNCTIONS
    func createDropDown(_ arr: [String], _ tf: UITextField) {
        dropDown.dataSource = arr
        dropDown.anchorView = tf
        dropDown.selectionAction = { [unowned self] (index, item) in
            tf.text = item
        }
        dropDown.show()
    }
    
    
    //MARK: WS_UPDATE_PROFILE
    func wsUpdateProfile() {
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["first_name"] = string(kAppDelegate.userInfo, "first_name")
        params["last_name"] = string(kAppDelegate.userInfo, "last_name")
        params["broker"] = string(kAppDelegate.userInfo, "broker")
        params["email"] = string(kAppDelegate.userInfo, "email")
        params["mobile"] = string(kAppDelegate.userInfo, "mobile")
        params["broker"] = tfBroker.text ?? ""
        params["offer"] = tfTagLine.text ?? ""
        params["close_deals"] = tfCloseDeals.text ?? ""
        params["agent_since"] = tfAgentSince.text ?? ""
        
        showHUD("Loading...".getLoclized())
        let api = api_user_update
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                            kAppDelegate.userType = UT_AGENT
                            let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                            UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                            Http.alert(appName, AlertMsg.updateSuccess)
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    
    
}//Class End
