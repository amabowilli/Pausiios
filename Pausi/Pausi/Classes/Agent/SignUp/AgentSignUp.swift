//
//  AgentSignUp.swift
//  Pausi
//
//  Created by mac on 19/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class AgentSignUp: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfBroker: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var imgProfile: ImageView!
    @IBOutlet weak var imgUpload: UIImageView!
    
    
    //MARK: VARIABLES
    var isAccepted = false
    var isProfile = true
    
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        showDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionProfile(_ sender: Any) {
        openFileAttachment()
        isProfile = true
    }
    
    @IBAction func actionUpdateId(_ sender: Any) {
        openFileAttachment()
        isProfile = false
    }
    
    @IBAction func actionAccept(_ sender: Any) {
        isAccepted = !isAccepted
        if isAccepted {
            btnAccept.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        } else {
            btnAccept.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func actionConfirm(_ sender: Any) {
        if let strMsg = checkValidation() {
            Http.alert(appName, strMsg)
        } else {
            wsSignUp()
        }
    }
    
    @IBAction func actionPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/privacy_policy.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func actionTerms(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/terms_service.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    //MARK: FUNCTIONS
    func showDetails() {
        tfFirstName.text = string(kAppDelegate.userInfo, "first_name")
        tfLastName.text = string(kAppDelegate.userInfo, "last_name")
        tfEmail.text = string(kAppDelegate.userInfo, "email")
        tfNumber.text = string(kAppDelegate.userInfo, "mobile")
    }
    
    func checkValidation() -> String? {
        //imgIdProof
        if imgProfilePicture == nil {
            return AlertMsg.selectPhoto
        } else if tfFirstName.text?.count == 0 {
            return AlertMsg.blankFirstName
        } else if tfLastName.text?.count == 0 {
            return AlertMsg.blankLastName
        } else if imgIdProof == nil {
            return AlertMsg.selectID
        } else if tfBroker.text?.count == 0 {
            return AlertMsg.blankBroker
        } else if tfEmail.text?.count == 0 {
            return AlertMsg.blankEmail
        } else if !(tfEmail.text!.isEmail) {
            return AlertMsg.invalidEmail
        } else if tfNumber.text?.count == 0 {
            return AlertMsg.blankNumber
        } else if tfPassword.text?.count == 0 {
            return AlertMsg.blankPassword
        } else if !(isAccepted) {
            return AlertMsg.acceptTerms
        }
        return nil
    }
    
    func openFileAttachment() {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".getLoclized(), style: UIAlertActionStyle.cancel, handler: nil))
        
        if PredefinedConstants.deviceType == .pad {
            actionSheet.popoverPresentationController?.sourceView = self.view
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK: PICKERVIEW DELEGATE
    var imgProfilePicture:UIImage? = nil
    var imgIdProof:UIImage? = nil

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var img:UIImage? = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let iii = info[UIImagePickerControllerEditedImage] as? UIImage {
            img = iii
        }
        if (img != nil) {
            if isProfile {
                imgProfilePicture = img
                imgProfile.image = img
            } else {
                imgIdProof = img
                imgUpload.image = img
            }
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: WS_SIGN_UP
    func wsSignUp() {

        showHUD("Loading...".getLoclized())
        let params = [
            "first_name": tfFirstName.text!,
            "last_name": tfLastName.text!,
            "device_type": "Ios",
            "broker": tfBroker.text!,
            "email": tfEmail.text!,
            "mobile": tfNumber.text!,
            "password": tfPassword.text!,
            "user_type": kAppDelegate.userType,
            "country_code": "",
            "status": "Online",
            "lat": "\(String(describing: kAppDelegate.currentLocation.coordinate.latitude))",
            "lon": "\(String(describing: kAppDelegate.currentLocation.coordinate.longitude))",
            "ios_device_token": kAppDelegate.token
            ] as [String : AnyObject]

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if self.imgProfilePicture != nil {
             multipartFormData.append(UIImageJPEGRepresentation(self.imgProfilePicture!, 0.5)!, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
             }
            
            if self.imgIdProof != nil {
                multipartFormData.append(UIImageJPEGRepresentation(self.imgIdProof!, 0.5)!, withName: "id_proof", fileName: "id_proof.jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: api_signup)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("progress-\(progress)-")
                })
                
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        self.hideHUD()
                        print("url-\(api_signup)-")
                        print("params-\(params)-")
                        if let json = response.result.value as? NSDictionary {
                            print("responce-\(json)-")
                            
                            if number(json, "status").boolValue {
                                if let result = json.object(forKey: "result") as? NSDictionary {
                                    self.fireBaseSignup()
                                    kAppDelegate.userId = string(result, "id")
                                    kAppDelegate.userType = string(result, "user_type")
                                    kAppDelegate.profileUrl = string(result, "image")
                                    kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                                    UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                                    kAppDelegate.window?.rootViewController = vc
                                    self.navigationController?.viewControllers = []
                                }
                            } else {
                                Http.alert(appName, string(json, "message"))
                            }
                            
                        } else {
                            Http.alert(appName, response.error?.localizedDescription)
                        }
                    }
                }
            case .failure(let encodingError):
                //Http.alert(appName, encodingError.localizedDescription)
                print("responce-\(encodingError.localizedDescription)-")
            }
        }
    }
    
    func fireBaseSignup() {
        User.registerUser(withName: "\(tfFirstName.text!) \(tfLastName.text!)", email: tfEmail.text!, password: "qwerty") { [weak weakSelf = self] (status, error) in
            DispatchQueue.main.async {
                
                if status == true {
                    print("fireBaseSignup sucess")
                }
            }
        }
    }
    
}//Class End
