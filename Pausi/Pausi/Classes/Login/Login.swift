//
//  Login.swift
//  Pausi
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class Login: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    //MARK: VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSkip(_ sender: Any) {
        kAppDelegate.userId = ""
        kAppDelegate.userType = ""
        kAppDelegate.profileUrl = ""
        kAppDelegate.userInfo.removeAllObjects()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
        kAppDelegate.window?.rootViewController = vc
        self.navigationController?.viewControllers = []
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUp") as! SignUp
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionForgotPassword(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassword") as! ForgotPassword
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionFb(_ sender: Any) {
        fbLogin ()
    }
    
    @IBAction func actionGoogle(_ sender: Any) {
        googleLogin()
    }
    
    @IBAction func actionYahoo(_ sender: Any) {
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        if let strMsg = checkValidation() {
            Http.alert(appName, strMsg)
        } else {
            wsLogin()
        }
    }
    
    //MARK: FUNCTIONS
    func checkValidation() -> String? {
        if tfEmail.text?.count == 0 {
            return AlertMsg.blankEmail
        } else if !(tfEmail.text!.isEmail) {
            return AlertMsg.invalidEmail
        } else if tfPassword.text?.count == 0 {
            return AlertMsg.blankPassword
        }
        return nil
    }
    
    func fireBaseLogin(_ dict: NSDictionary) {
        User.loginUser(withEmail: self.tfEmail.text!, password: "qwerty") { [weak weakSelf = self](status, error) in
            DispatchQueue.main.async {
                if error == nil {
                    if status == true {
                        print("fireBaseLogin Success")
                    }
                } else {
                    if error?.localizedDescription == "There is no user record corresponding to this identifier. The user may have been deleted." {
                        self.fireBaseSignup(dict)
                    }
                }
            }
        }
    }
    
    func fireBaseSignup(_ dict: NSDictionary) {
        User.registerUser(withName: "\(string(dict, "first_name")) \(string(dict, "last_name"))", email: string(dict, "email"), password: "qwerty") { [weak weakSelf = self] (status, error) in
            DispatchQueue.main.async {
                
                if status == true {
                    print("fireBaseSignup sucess")
                }
            }
        }
    }
    
    //MARK: WS_LOGIN
    func wsLogin() {

        let params = NSMutableDictionary()
        params["email"] = tfEmail.text!
        params["device_type"] = "Ios"
        params["password"] = tfPassword.text!
        params["lat"] = "\(String(describing: kAppDelegate.currentLocation.coordinate.latitude))"
        params["lon"] = "\(String(describing: kAppDelegate.currentLocation.coordinate.longitude))"
        params["ios_device_token"] = kAppDelegate.token
        params["status"] = "Online"

        showHUD("Loading...".getLoclized())
        let api = api_login
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            self.fireBaseLogin(result)
                            kAppDelegate.userId = string(result, "id")
                            kAppDelegate.userType = string(result, "user_type")
                            kAppDelegate.profileUrl = string(result, "image")
                            kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                            let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                            UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                            kAppDelegate.window?.rootViewController = vc
                            self.navigationController?.viewControllers = []
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    /***************************** Facebook  ******************************/
    
    func fbLogin () {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil) {
                if ((FBSDKAccessToken.current()) != nil) {
                    self.showHUD("Loading...".getLoclized())
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                        if (error == nil) {
                            if let dt = result as? NSDictionary {
                                
                                let md = NSMutableDictionary()
                                
                                md["first_name"] = dt["first_name"]
                                md["last_name"] = dt["last_name"]
                                md["email"] = dt["email"]
                                md["social_id"] = dt["id"]
                                md["user_type"] = "USER"
                                md["device_type"] = "Ios"
                                md["status"] = "Online"
                                md["ios_device_token"] = kAppDelegate.token
                                md["mobile"] = ""
                                md["lat"] = "\(kAppDelegate.currentLocation.coordinate.latitude)"
                                md["lon"] = "\(kAppDelegate.currentLocation.coordinate.longitude)"
                                var url:String? = nil
                                
                                if let picture = dt["picture"] as? NSDictionary {
                                    if let data = picture["data"] as? NSDictionary {
                                        if let uurl = data["url"] as? String {
                                            url = uurl
                                        }
                                    }
                                }
                                
                                if url == nil {
                                    self.socialLogin(md)
                                } else {
                                    //md["social_dp"] = url!
                                    self.socialLogin(md)
                                }
                                print(md)//Print FB User Details
                            } else {
                                self.stopActivityI()
                            }
                        } else {
                            self.stopActivityI()
                        }
                    })
                } else {
                    self.stopActivityI()
                }
            }
        }
    }
    
    //MARK: WS_LOGIN
    func socialLogin (_ md:NSMutableDictionary) {
        
        hideHUD()

        showHUD("Loading...".getLoclized())
        let api = api_social_login
        Alamofire.request(api, method: .get, parameters: md as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(md)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            self.fireBaseSignup(md)
                            kAppDelegate.userId = string(result, "id")
                            kAppDelegate.userType = string(result, "user_type")
                            kAppDelegate.profileUrl = string(result, "image")
                            kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                            let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                            UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                            
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                            kAppDelegate.window?.rootViewController = vc
                            self.navigationController?.viewControllers = []
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    func fireBaseSignup(_ md:NSMutableDictionary) {
        
        User.registerUser(withName: "\(string(md, "first_name")) \(string(md, "last_name"))", email: string(md, "email"), password: "qwerty") { [weak weakSelf = self] (status, error) in
            DispatchQueue.main.async {
                if error == nil {
                    if status == true {
                        print("fireBaseSignup sucess")
                    }
                } else {
                    print(error?.localizedDescription)
                    if error?.localizedDescription == "The email address is already in use by another account." {
                        self.fireBaseSocialLogin(md)
                    }
                }
            }
        }
    }
    
    func fireBaseSocialLogin(_ md:NSMutableDictionary) {
        User.loginUser(withEmail: string(md, "email"), password: "qwerty") { [weak weakSelf = self](status, error) in
            DispatchQueue.main.async {
                
                if status == true {
                    print("fireBaseLogin Success")
                }
            }
        }
    }
    
    func googleLogin () {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            showHUD("Loading...".getLoclized())
            
            let userId = user.userID
            //let idToken = user.authentication.idToken
            //let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            let md = NSMutableDictionary()
            //md["username"] = "\(givenName!) \(familyName!)"
            md["first_name"] = givenName
            md["last_name"] = familyName
            md["email"] = email
            md["social_id"] = userId
            md["user_type"] = "USER"
            md["device_type"] = "Ios"
            md["status"] = "Online"
            md["ios_device_token"] = kAppDelegate.token
            md["mobile"] = ""
            md["lat"] = "\(kAppDelegate.currentLocation.coordinate.latitude)"
            md["lon"] = "\(kAppDelegate.currentLocation.coordinate.longitude)"
            
            var url:String? = nil
            
            if user.profile.hasImage {
                let uurl = user.profile.imageURL(withDimension: 120)
                url = uurl?.absoluteString
            }
            
            if url == nil {
                self.socialLogin(md)
            } else {
                //md["social_dp"] = url!
                self.socialLogin(md)
            }
            print(md)//Print Google User Detail from Google Api
        } else {
            print("1 sign error-\(error.localizedDescription)")
            stopActivityI()
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        if error != nil {
            hideHUD()
        }
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        hideHUD()
    }
    
    func stopActivityI () {
        hideHUD()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        stopActivityI()
    }
    
    
}//Class End
