//
//  AddProperty.swift
//  Pausi
//
//  Created by mac on 04/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import DropDown
import GoogleMaps
import GooglePlacePicker
import Alamofire

class AddProperty: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PropertyCellDelegate, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, CroppedImageDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfPropertyTitle: UITextField!
    @IBOutlet weak var tfPropertyFor: UITextField!
    @IBOutlet weak var tfAvailableFor: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfStreet: UITextField!
    @IBOutlet weak var tfPropertyType: UITextField!
    @IBOutlet weak var tfBeds: UITextField!
    @IBOutlet weak var tfBaths: UITextField!
    @IBOutlet weak var tfSize: UITextField!
    @IBOutlet weak var tfYear: UITextField!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tvDescription: TextView!
    @IBOutlet weak var cvView: UICollectionView!
    @IBOutlet weak var heightCharacterstics: NSLayoutConstraint!
    @IBOutlet weak var vwCharacterstics: UIView!
    @IBOutlet weak var btnKitchen: UIButton!
    @IBOutlet weak var btnDining: UIButton!
    @IBOutlet weak var btnParking: UIButton!
    @IBOutlet weak var btnGarage: UIButton!
    @IBOutlet weak var btnStudy: UIButton!
    
    
    //MARK: VARIABLES
    var arrImages = NSMutableArray()
    var dropDown = DropDown()
    var arrYears = [String]()
    var arrPropertyType = NSMutableArray()
    var arrPropertyFor = NSMutableArray()
    var arrSaleStatus = NSMutableArray()
    var arrRentStatus = NSMutableArray()
    var lat: Double = 0.0
    var lon: Double = 0.0
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        createArray()
        heightCharacterstics.constant = -32.0
        vwCharacterstics.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPropertyFor(_ sender: Any) {
        dropDown.dataSource.removeAll()
        for i in 0..<arrPropertyFor.count {
            let dict = arrPropertyFor.object(at: i) as! NSDictionary
            dropDown.dataSource.append(string(dict, "key"))
        }
        dropDown.anchorView = tfPropertyFor
        dropDown.selectionAction = {[unowned self] (index, item) in
            if self.tfPropertyFor.text != item {
                self.tfPropertyFor.text = item
                self.tfAvailableFor.text = ""
                for i in 0..<self.arrPropertyFor.count {
                    let dict = self.arrPropertyFor.object(at: i) as! NSDictionary
                    if string(dict, "key") == item {
                        self.propertyForValue = string(dict, "value")
                    }
                }
            }
        }
        dropDown.show()
    }
    
    @IBAction func actionAvailableFor(_ sender: Any) {
        dropDown.dataSource.removeAll()
        if tfPropertyFor.text == "Sale".getLoclized() {
            for i in 0..<arrSaleStatus.count {
                let dict = arrSaleStatus.object(at: i) as! NSDictionary
                dropDown.dataSource.append(string(dict, "key"))
            }
        } else {
            for i in 0..<arrRentStatus.count {
                let dict = arrRentStatus.object(at: i) as! NSDictionary
                dropDown.dataSource.append(string(dict, "key"))
            }
        }
        dropDown.anchorView = tfAvailableFor
        dropDown.selectionAction = {[unowned self] (index, item) in
            self.tfAvailableFor.text = item
            if self.tfPropertyFor.text == "Sale".getLoclized() {
                for i in 0..<self.arrSaleStatus.count {
                    let dict = self.arrSaleStatus.object(at: i) as! NSDictionary
                    if string(dict, "key") == item {
                        self.availableForValue = string(dict, "value")
                    }
                }
            } else {
                for i in 0..<self.arrRentStatus.count {
                    let dict = self.arrRentStatus.object(at: i) as! NSDictionary
                    if string(dict, "key") == item {
                        self.availableForValue = string(dict, "value")
                    }
                }
            }
        }
        dropDown.show()
    }
    
    @IBAction func actionAddress(_ sender: Any) {
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        present(placePickerController, animated: true, completion: nil)
    }
    
    @IBAction func actionPropertyType(_ sender: Any) {
        dropDown.dataSource.removeAll()
        for i in 0..<arrPropertyType.count {
            let dict = arrPropertyType.object(at: i) as! NSDictionary
            dropDown.dataSource.append(string(dict, "key"))
        }
        dropDown.anchorView = tfPropertyType
        dropDown.selectionAction = {[unowned self] (index, item) in
            self.tfPropertyType.text = item
            for i in 0..<self.arrPropertyType.count {
                let dict = self.arrPropertyType.object(at: i) as! NSDictionary
                if item == string(dict, "key") {
                    self.propertyTypeValue = string(dict, "value")
                }
            }
            if item == "Apartment" || item == "Appartement"  {
                self.heightCharacterstics.constant = 123.0
                self.vwCharacterstics.isHidden = false
            } else {
                self.heightCharacterstics.constant = -32.0
                self.vwCharacterstics.isHidden = true
            }
        }
        dropDown.show()
    }
    
    var arrEminities = NSMutableArray()
    var isKitchen = false
    @IBAction func actionKitchen(_ sender: Any) {
        isKitchen = !isKitchen
        btnKitchen.setImage(getSelectImg(isKitchen, "Kitchen"), for: .normal)
    }
    
    var isDining = false
    @IBAction func actionDining(_ sender: Any) {
        isDining = !isDining
        btnDining.setImage(getSelectImg(isDining, "Dining"), for: .normal)
    }
    
    var isParking = false
    @IBAction func actionParking(_ sender: Any) {
        isParking = !isParking
        btnParking.setImage(getSelectImg(isParking, "Parking"), for: .normal)
    }
    
    var isGarage = false
    @IBAction func actionGarage(_ sender: Any) {
        isGarage = !isGarage
        btnGarage.setImage(getSelectImg(isGarage, "External Garage"), for: .normal)
    }
    
    var isStudy = false
    @IBAction func actionStudy(_ sender: Any) {
        isStudy = !isStudy
        btnStudy.setImage(getSelectImg(isStudy, "Study"), for: .normal)
    }
    
    @IBAction func actionYear(_ sender: Any) {
        dropDown.dataSource = arrYears
        dropDown.anchorView = tfYear
        dropDown.selectionAction = {[unowned self] (index, item) in
            self.tfYear.text = item
        }
        dropDown.show()
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        if let strMsg = checkValidation() {
            Http.alert(appName, strMsg)
        } else {
            wsAddProperty()
        }
    }
    
    func actionRemoveImage(index: IndexPath) {
        arrImages.removeObject(at: index.row)
        cvView.reloadData()
    }
    
    func actionAddImage(index: IndexPath) {
        openFileAttachment()
    }
    
    //MARK: FUNCTIONS
    func getSelectImg(_ isBool: Bool, _ title: String) -> UIImage {
        if isBool {
            self.arrEminities.add(title)
            return UIImage(named: "check")!
        } else {
            self.arrEminities.remove(title)
            return UIImage(named: "uncheck")!
        }
    }
    
    func createArray() {
        arrImages.add(#imageLiteral(resourceName: "add.png"))
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        for i in (1950...year).reversed() {
            //arrYears.append("Agent Since \(i)")
            arrYears.append("\("Agent Since".getLoclized()) \(i)")
        }
        arrPropertyFor = [["key":"Sale".getLoclized(), "value": "Sale"], ["key":"Rent".getLoclized(), "value": "Rent"]]
        arrPropertyType = [["key":"Family Homes".getLoclized(), "value": "Family Homes"], ["key":"Apartment".getLoclized(), "value": "Apartment"], ["key":"Plots and Land".getLoclized(), "value": "Plots and Land"], ["key":"Guest House".getLoclized(), "value": "Guest House"], ["key":"Commercial".getLoclized(), "value": "Commercial"], ["key":"Others".getLoclized(), "value": "Others"]]
        arrSaleStatus = [["key":"FOR SALE".getLoclized(), "value": "FOR SALE"], ["key":"OPEN HOUSE".getLoclized(), "value": "OPEN HOUSE"], ["key":"CONTRACT".getLoclized(), "value": "CONTRACT"]]
        arrRentStatus = [["key":"FOR RENT".getLoclized(), "value": "FOR RENT"], ["key":"OPEN HOUSE".getLoclized(), "value": "OPEN HOUSE"], ["key":"PENDING".getLoclized(), "value": "PENDING"]]
    }
    
    func checkValidation() -> String? {
        if tfPropertyTitle.text?.count == 0 {
            return AlertMsg.blankPropertyTitle
        } else if tfPropertyFor.text?.count == 0 {
            return AlertMsg.selectPropertyFor
        } else if tfAvailableFor.text?.count == 0 {
            return AlertMsg.selectAvailableFor
        } else if tfAddress.text?.count == 0 {
            return AlertMsg.blankAddress
        } else if tfStreet.text?.count == 0 {
            return AlertMsg.blankLendMark
        } else if tfPropertyType.text?.count == 0 {
            return AlertMsg.blankPropertyType
        } else if tfBeds.text?.count == 0 {
            return AlertMsg.blankBed
        } else if tfBaths.text?.count == 0 {
            return AlertMsg.blankBath
        } else if tfSize.text?.count == 0 {
            return AlertMsg.blsnkSize
        } else if tfYear.text?.count == 0 {
            return AlertMsg.blankBuiltIn
        } else if tfPrice.text?.count == 0 {
            return AlertMsg.blankPrice
        } else if tvDescription.text.count == 0 {
            return AlertMsg.blankDescription
        } else if arrImages.count <= 1 {
            return AlertMsg.selectImage
        }
        return nil
    }
    
    func openFileAttachment() {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".getLoclized(), style: UIAlertActionStyle.cancel, handler: nil))
        if PredefinedConstants.deviceType == .pad {
            actionSheet.popoverPresentationController?.sourceView = self.view
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            //imagePicker.allowsEditing = true
            
            imagePicker.allowsEditing = false
            imagePicker.showsCameraControls=true
            imagePicker.modalPresentationStyle = .currentContext
            imagePicker.modalPresentationStyle = .fullScreen
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            //imagePicker.allowsEditing = true
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK: PICKERVIEW DELEGATE
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var img:UIImage? = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let iii = info[UIImagePickerControllerEditedImage] as? UIImage {
            img = iii
        }
        if (img != nil) {
            picker.dismiss(animated: false, completion: nil);
            /*let vc = self.storyboard?.instantiateViewController(withIdentifier: "CropperVC") as! CropperVC
            vc.newImage = img
            vc.delegate = self
            self.present(vc, animated: false, completion: nil)*/
            self.arrImages.insert(img!, at: 1)
            self.cvView.reloadData()
        }
        
    }
    
    func getCroppedImage(img: UIImage) {
        self.arrImages.insert(img, at: 1)
        self.cvView.reloadData()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    var propertyTypeValue = ""
    var availableForValue = ""
    var propertyForValue = ""
    
    //MARK: WS_ADD_PROPERTY
    func wsAddProperty() {

        showHUD("Loading...".getLoclized())
        let params = [
            "user_id": kAppDelegate.userId,
            "property_name": tfPropertyTitle.text!,
            "property_price": tfPrice.text!,
            "modified_price": tfPrice.text!,
            "property_type": propertyTypeValue,
            "status": availableForValue,
            "sq_feet": tfSize.text!,
            "beds": tfBeds.text!,
            "baths": tfBaths.text!,
            "build_year": tfYear.text!,
            "acre_area": tfSize.text!,
            "country": "",
            "state": "",
            "city": "",
            "address": tfAddress.text!,
            "lat": "\(lat)",
            "lon": "\(lon)",
            "zipcode": "",
            "sale_type": propertyForValue,
            "property_description": tvDescription.text!,
            "image_cnt": "\(arrImages.count - 1)",
            "address_2": tfStreet.text!,
            "property_amenities": arrEminities.componentsJoined(by: ","),
            "language": kAppDelegate.currentLanguage
            ]
                
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for i in 0..<self.arrImages.count {
                if i > 0 {
                    if let img = self.arrImages[i] as? UIImage {
                        multipartFormData.append(UIImageJPEGRepresentation(img, 0.5)!, withName: "property_image[]", fileName: "property_image.jpeg", mimeType: "image/jpeg")
                    }
                }
            }
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: api_add_new_property)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("progress-\(progress)-")
                })
                
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        self.hideHUD()
                        print("url-\(api_add_new_property)-")
                        print("params-\(params)-")
                        
                        if let json = response.result.value as? NSDictionary {
                            print("responce-\(json)-")
                            
                            if number(json, "status").boolValue {
                                Http.alert(appName, AlertMsg.updateSuccess)
                                DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                                    _=self.navigationController?.popViewController(animated: true)
                                })
                            } else {
                                Http.alert(appName, string(json, "result"))
                            }
                            
                        } else {
                            Http.alert(appName, response.error?.localizedDescription)
                        }
                    }
                }
            case .failure(let encodingError):
                //Http.alert(appName, encodingError.localizedDescription)
                print("responce-\(encodingError.localizedDescription)-")
            }
        }
    }
    
    
    //MARK: COLLECTIONVIEW DELEGATE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PropertyCell", for: indexPath) as! PropertyCell
        cell.delegate = self
        cell.index = indexPath
        cell.imgView.image = arrImages[indexPath.row] as? UIImage
        if indexPath.row == 0 {
            cell.btnCancel.isHidden = true
            cell.btnProfile.isEnabled = true
        } else {
            cell.btnCancel.isHidden = false
            cell.btnProfile.isEnabled = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 180)
    }
    
    //MARK: AUTOCOMPLETEDVIEW DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        lat = place.coordinate.latitude
        lon = place.coordinate.longitude
        tfStreet.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}//Class End
