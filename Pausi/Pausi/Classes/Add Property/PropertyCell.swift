//
//  PropertyCell.swift
//  Pausi
//
//  Created by mac on 04/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol PropertyCellDelegate {
    func actionRemoveImage(index: IndexPath)
    func actionAddImage(index: IndexPath)
}

class PropertyCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    
    var index: IndexPath!
    var delegate: PropertyCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func actionAddProfile(_ sender: Any) {
        delegate.actionAddImage(index: index)
    }
    
    @IBAction func actionRemoveImage(_ sender: Any) {
        delegate.actionRemoveImage(index: index)
    }
}
