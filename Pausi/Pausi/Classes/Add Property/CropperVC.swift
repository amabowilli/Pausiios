//
//  CropperVC.swift
//  Pausi
//
//  Created by mac on 17/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol CroppedImageDelegate {
    func getCroppedImage(img: UIImage)
}

class CropperVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
            scrollView.minimumZoomScale = 1.0
            scrollView.maximumZoomScale = 10.0
        }
    }
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var cropAreaView: CropAreaView!
    
    var newImage: UIImage?
    var delegate: CroppedImageDelegate!
    
    var cropArea:CGRect{
        get{
            /*let factorWidth = imageView.image!.size.width/scrollView.frame.width
            let factorHeight = imageView.image!.size.height/scrollView.frame.height
            let scale = 1/scrollView.zoomScale
            let imageFrame = imageView.imageFrame()
            
            let x = (scrollView.contentOffset.x + cropAreaView.frame.origin.x - imageFrame.origin.x) * scale * factorWidth
            let y = ((scrollView.contentOffset.y + cropAreaView.frame.origin.y - imageFrame.origin.y) * scale * factorHeight)
            //let x = (scrollView.contentOffset.x - imageFrame.origin.x) * scale * factor
            //let y = (scrollView.contentOffset.y - imageFrame.origin.y) * scale * factor
            //let width = scrollView.frame.size.width//cropAreaView.frame.size.width * scale * factor
            let width = cropAreaView.frame.size.width * scale * factorWidth
            //let height = scrollView.frame.size.height//cropAreaView.frame.size.height * scale * factor
            let height = cropAreaView.frame.size.height * scale * factorHeight*/
            let factor = imageView.image!.size.width/view.frame.width
            let scale = 1/scrollView.zoomScale
            let imageFrame = imageView.imageFrame1()
            let x = (scrollView.contentOffset.x + cropAreaView.bounds.origin.x - imageFrame.origin.x) * scale * factor
            let y = (scrollView.contentOffset.y + cropAreaView.bounds.origin.y - imageFrame.origin.y) * scale * factor
            let width =  cropAreaView.bounds.width  * scale * factor
            let height = cropAreaView.bounds.height  * scale * factor
            return CGRect(x: x, y: y, width: width, height: height)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cropAreaView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        imageView.image = newImage
        //imgCover.uiimage(string(dictDetail, "image1"), "", false, nil)
    }
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func crop(_ sender: UIButton) {
        /*imageView.isHidden = true
        let croppedCGImage = imageView.image?.cgImage?.cropping(to: cropArea)
        let croppedImage = UIImage(cgImage: croppedCGImage!)
        scrollView.zoomScale = 1*/
        
        UIGraphicsBeginImageContextWithOptions(self.scrollView.bounds.size, false, UIScreen.main.scale)
        let offset = self.scrollView.contentOffset
        let thisContext = UIGraphicsGetCurrentContext()
        thisContext?.translateBy(x: -offset.x, y: -offset.y)
        self.scrollView.layer.render(in: thisContext!)
        let visibleScrollViewImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        /*let imag = UIImageView()
        imag.frame = CGRect(x: 0.0, y: 70.0, width: 200, height: 300)
        imag.image = visibleScrollViewImage
        self.view.addSubview(imag)*/
        delegate.getCroppedImage(img: visibleScrollViewImage!)
        self.dismiss(animated: false, completion: nil)
    }
    
}

extension UIImageView{
    func imageFrame()->CGRect{
        let imageViewSize = self.frame.size
        guard let imageSize = self.image?.size else{return CGRect.zero}
        let imageRatio = imageSize.width / imageSize.height
        let imageViewRatio = imageViewSize.width / imageViewSize.height
        
        if imageRatio < imageViewRatio {
            let scaleFactor = imageViewSize.height / imageSize.height
            let width = imageSize.width * scaleFactor
            let topLeftX = (imageViewSize.width - width) * 0.5
            return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
        }else{
            let scalFactor = imageViewSize.width / imageSize.width
            let height = imageSize.height * scalFactor
            let topLeftY = (imageViewSize.height - height) * 0.5
            return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
        }
    }
}


extension UIImageView
{
    func imageFrame1()->CGRect
    {
        let imageViewSize = self.frame.size
        guard let imageSize = self.image?.size else
        {
            return CGRect.zero
        }
        let imageRatio = imageSize.width / imageSize.height
        let imageViewRatio = imageViewSize.width / imageViewSize.height
        if imageRatio < imageViewRatio
        {
            let scaleFactor = imageViewSize.height / imageSize.height
            let width = imageSize.width * scaleFactor
            let topLeftX = (imageViewSize.width - width) * 0.5
            return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
        }
        else
        {
            let scalFactor = imageViewSize.width / imageSize.width
            let height = imageSize.height * scalFactor
            let topLeftY = (imageViewSize.height - height) * 0.5
            return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
        }
    }
}



class CropAreaView: UIView {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return false
    }
    
}
