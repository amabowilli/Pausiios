//
//  ForgotPassword.swift
//  Pausi
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPassword: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfEmail: UITextField!
    
    //MARK: VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        if let strMsg = checkValidation() {
            Http.alert(appName, strMsg)
        } else {
            wsForgotPassword()
        }
    }
    
    //MARK: FUNCTIONS
    func checkValidation() -> String? {
        if tfEmail.text?.count == 0 {
            return AlertMsg.blankEmail
        } else if !(tfEmail.text!.isEmail) {
            return AlertMsg.invalidEmail
        }
        return nil
    }
    
    //MARK: WS_FORGOT_PASSWORD
    func wsForgotPassword() {

        let params = NSMutableDictionary()
        params["email"] = tfEmail.text!
        
        showHUD("Loading...".getLoclized())
        let api = api_forgot_password
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        Http.alert(appName, AlertMsg.submitSuccess)
                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                            _=self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
}//Class End

