//
//  LandingCell.swift
//  Pausi
//
//  Created by mac on 20/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class LandingCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
