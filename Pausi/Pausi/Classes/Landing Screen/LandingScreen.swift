//
//  LandingScreen.swift
//  Pausi
//
//  Created by mac on 20/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class LandingScreen: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    //MARK: OUTLETS
    @IBOutlet var vwLanguage: UIView!
    @IBOutlet weak var cvView: UICollectionView!
    @IBOutlet weak var pgControl: UIPageControl!

    //MARK: VARIABLES
    var arrSlider = NSMutableArray()
    var timer = Timer()
    var count = 0

    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        vwLanguage.frame = CGRect(x: 0.0, y: 0.0, width: PredefinedConstants.ScreenWidth, height: PredefinedConstants.ScreenHeight)
        vwLanguage.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(swipePages), userInfo: nil, repeats: true)
        crateArray()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionEnglish(_ sender: Any) {
        L102Language.setAppleLAnguageTo(lang: "en")
        vwLanguage.removeFromSuperview()
        //let _ = "Camera".localized("en")
        jumpToLogin()
    }
    
    @IBAction func actionFrench(_ sender: Any) {
        L102Language.setAppleLAnguageTo(lang: "fr")
        vwLanguage.removeFromSuperview()
        //let _ = "Camera".localized("fr")
        jumpToLogin()
    }
    
    func jumpToLogin() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionGetStarted(_ sender: Any) {
        self.view.addSubview(vwLanguage)
    }
    
    //MARK: FUNCTIONS
    func crateArray() {
        arrSlider.add(["title": "Keep track of your favorite homes".getLoclized(), "image": #imageLiteral(resourceName: "p1.png")])
        arrSlider.add(["title": "Share homes with your friends or agent".getLoclized(), "image": #imageLiteral(resourceName: "p2.png")])
        arrSlider.add(["title": "Save your favorite searches".getLoclized(), "image": #imageLiteral(resourceName: "p3.png")])
        arrSlider.add(["title": "View full property history".getLoclized(), "image": #imageLiteral(resourceName: "p4.png")])
        arrSlider.add(["title": "Follow homes to get notified of price changes".getLoclized(), "image": #imageLiteral(resourceName: "p5.png")])
        pgControl.numberOfPages = arrSlider.count
        self.cvView.reloadData()
        //wsGetImages()
    }
    
    @objc func swipePages() {
        count = (count < (arrSlider.count - 1)) ? (count + 1) : 0
        cvView.scrollToItem(at: IndexPath(item: count, section: 0), at: .right, animated: true)
    }
    
    //MARK: UICOLLECTION VIEW DELEGATE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSlider.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LandingCell", for: indexPath) as! LandingCell
        let dict = arrSlider.object(at: indexPath.row) as! NSDictionary
        cell.lblTitle.text = dict.object(forKey: "title") as? String
        cell.imgView.image = dict.object(forKey: "image") as? UIImage
        //cell.imgView.sd_setImage(with: URL(string: string(dict, "image")), placeholderImage: #imageLiteral(resourceName: "bg"), options: SDWebImageOptions(rawValue: 1), completed: nil)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: PredefinedConstants.ScreenWidth, height: PredefinedConstants.ScreenHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pgControl.currentPage = indexPath.row
    }
    
    
    //MARK: WS_GET_IMAGES
    func wsGetImages() {
        let params = NSMutableDictionary()
        
        showHUD("Loading...".getLoclized())
        let api = api_all_slider_image_list
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            for i in 0..<result.count {
                                let dict = result.object(at: i) as! NSDictionary
                                let index = ((self.arrSlider.count > i) ? i : (i - self.arrSlider.count))
                                let dict1 = (self.arrSlider.object(at: index) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                                dict1["image"] = string(dict, "slider_image")
                                self.arrSlider.replaceObject(at: index, with: dict1)
                            }
                            self.cvView.reloadData()
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    
    
}//Class End
