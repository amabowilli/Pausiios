//
//  PropertyDetail.swift
//  Pausi
//
//  Created by mac on 15/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Firebase

class PropertyDetail: UIViewController, UIScrollViewDelegate, UIWebViewDelegate, UITableViewDataSource, UITableViewDelegate, HomeTblCell1Delegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var topBgImg: NSLayoutConstraint!
    @IBOutlet weak var heightBgImg: NSLayoutConstraint!
    @IBOutlet weak var lblTotalImage: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPropertyType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var scView: UIScrollView!
    @IBOutlet weak var btnScHeader: UIButton!
    @IBOutlet weak var heightScHeaderBtn: NSLayoutConstraint!
    @IBOutlet weak var lblBeds: UILabel!
    @IBOutlet weak var lblBaths: UILabel!
    @IBOutlet weak var lblSqFt: UILabel!
    @IBOutlet weak var bottomBgImg: NSLayoutConstraint!
    @IBOutlet weak var lblYearBuild: UILabel!
    @IBOutlet weak var wvDescription: UIWebView!
    @IBOutlet weak var heightWebView: NSLayoutConstraint!
    @IBOutlet weak var imageAgent: UIImageView!
    @IBOutlet weak var lblAgentName: UILabel!
    @IBOutlet weak var lblAgentWork: UILabel!
    @IBOutlet weak var lblAgentDetail: UILabel!
    @IBOutlet weak var tfAgentNumber: UITextField!
    @IBOutlet weak var tblSimilerProperty: UITableView!
    @IBOutlet weak var heightTbl: NSLayoutConstraint!
    @IBOutlet weak var lblNotifyMe: UILabel!
    @IBOutlet weak var vwMsg: UIView!
    @IBOutlet weak var topTbl: NSLayoutConstraint!
    @IBOutlet weak var vwNotify: UIView!
    @IBOutlet weak var imgSwipeUp: UIImageView!
    @IBOutlet weak var lblEminities: UILabel!
    @IBOutlet weak var lblAminiti: UILabel!
    @IBOutlet weak var vwPriceHistory: UIView!
    @IBOutlet weak var tblPriceHistory: UITableView!
    @IBOutlet weak var heightPriceHistory: NSLayoutConstraint!
    
    //MARK: VARIABLES
    var propertyId = ""
    var imageCount = 0
    var imgBgHeight: CGFloat = 0.0
    var dictDetail = NSDictionary()
    var arrImages = NSArray()
    var arrOnlyImages = [UIImage]()
    var dictAgent = NSDictionary()
    var timer = Timer()
    var arrSimiler = NSMutableArray()
    var items = [User]()
    var arrPriceHistory = NSMutableArray()
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        imgSwipeUp.image = imgSwipeUp.image!.withRenderingMode(.alwaysTemplate)
        imgSwipeUp.tintColor = PredefinedConstants.appColor()
        imgBg.image = #imageLiteral(resourceName: "bg")
        wsPropertyDetail()
        wsAddRecently()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        imgBgHeight = heightBgImg.constant
        heightScHeaderBtn.constant = PredefinedConstants.ScreenHeight - (vwHeader.frame.origin.y + vwHeader.frame.size.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUpload(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionNotifyMe(_ sender: Any) {
        wsNotifyMe()
    }
    
    
    var isPushed = false
    @IBAction func actionAskQuestion(_ sender: Any) {
        isPushed = false
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            
            if string(dictAgent, "email") == string(kAppDelegate.userInfo, "email") {
                Http.alert(appName, AlertMsg.cantChat)
            } else {
                self.showHUD("")
                print("dictAgent-\(dictAgent)-")
                kAppDelegate.propertyChatID = propertyId
                if let id = Auth.auth().currentUser?.uid {
                    User.downloadAllUsers(exceptID: id, completion: {(user) in
                        DispatchQueue.main.async {
                            self.items.append(user)
                            for item in self.items {
                                if string(self.dictAgent, "email") == item.email {
                                    if !self.isPushed {
                                        self.isPushed = true
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! ChatVC
                                        vc.currentUser = item
                                        vc.strNumber = self.tfAgentNumber.text
                                        self.navigationController?.pushViewController(vc, animated: true)
                                        self.navigationController?.setNavigationBarHidden(false, animated: true)
                                    }
                                }
                            }
                            self.hideHUD()
                        }
                    })
                }
            }
        }
    }
    
    //MARK: FUNCTIONS
    func setDetails() {
        
        if let arrImg = dictDetail.object(forKey: "property_images") as? NSArray {
            for i in 0..<arrImg.count {
                let dict = arrImg.object(at: i) as! NSDictionary
                downloadImage(URL(string: string(dict, "property_image"))!)
            }
            arrImages = arrImg
            imageCount = arrImg.count
        }
        print("dictDetail-\(dictDetail)-")
        if string(dictDetail, "property_type") == "Apartment" {
            lblAminiti.text = "Amenities:"
            var eminity = ""
            if let arr = dictDetail.object(forKey: "property_amenities") as? NSArray {
                for i in 0..<arr.count {
                    if let dict = arr.object(at: i) as? NSDictionary {
                        if i == 0 {
                            eminity = string(dict, "amenities")
                        } else {
                            eminity = "\(eminity), \(string(dict, "amenities"))"
                        }
                    }
                }
            }
            if eminity != "" {
                lblEminities.text = eminity
            } else {
                lblEminities.text = "N/A"
            }
            
        } else {
            lblEminities.text = ""
            lblAminiti.text = ""
        }
        
        lblName.text = string(dictDetail, "property_name")
        lblPrice.text = "\(string(dictDetail, "property_price")) XAF"
        //lblPropertyType.text = "For \(string(dictDetail, "sale_type"))"
        lblPropertyType.text = "  \(string(dictDetail, "status"))  "
        if string(dictDetail, "status") == "SOLD" || string(dictDetail, "status") == "NOT AVAILABLE" {
            //topTbl.constant = -450
            lblPropertyType.backgroundColor = UIColor.red
            topTbl.constant = -(tblSimilerProperty.frame.origin.y - vwMsg.frame.origin.y + 20)
        }
        
        /*
         if (string(dictDetail, "status") == "SOLD") || (string(dictDetail, "status") == "NOT AVAILABLE") {
         cell.lblStatus.backgroundColor = UIColor.red
         } else {
         cell.lblStatus.backgroundColor = PredefinedConstants.greenColor()
         }
         */
        
        lblAddress.text = string(dictDetail, "address")
        lblTotalImage.text = "\(arrImages.count)"
        lblBaths.text = string(dictDetail, "baths")
        lblBeds.text = string(dictDetail, "beds")
        lblSqFt.text = string(dictDetail, "sq_feet")
        lblYearBuild.text = string(dictDetail, "build_year")
        wvDescription.loadHTMLString(string(dictDetail, "property_description"), baseURL: nil)
        if string(dictDetail, "notified") == "NO" {
            lblNotifyMe.text = "Notify me for latest updates".getLoclized()
        } else {
            lblNotifyMe.text = "Turn off notifications".getLoclized()
        }
        if arrImages.count > 0 {
            let dictImage = arrImages.object(at: 0) as! NSDictionary
            self.imgBg.sd_setImage(with: URL(string: string(dictImage, "property_image")), placeholderImage: #imageLiteral(resourceName: "bg"), options: SDWebImageOptions(rawValue: 1), completed: nil)
        }

        if let dictAgent = dictDetail.object(forKey: "agent_details") as? NSDictionary {
            self.dictAgent = dictAgent
            self.imageAgent.sd_setImage(with: URL(string: "http://technorizen.com/WORKSPACE1/pausi/uploads/images/"+string(dictAgent, "image")), placeholderImage: #imageLiteral(resourceName: "profile"), options: SDWebImageOptions(rawValue: 1), completed: nil)
            self.lblAgentName.text = "\(string(dictAgent, "first_name")) \(string(dictAgent, "last_name"))"
            self.lblAgentWork.text = string(dictAgent, "broker")
            self.lblAgentDetail.text = "I found \(string(dictDetail, "property_name")), \(string(dictDetail, "address")) on Pausi and would like to learn more about it. Thanks!"
        }
        
        if let arr = dictDetail.object(forKey: "similar_listing_list") as? NSArray {
            arrSimiler = arr.mutableCopy() as! NSMutableArray
            self.tblSimilerProperty.reloadData()
            let cellHeight = ((PredefinedConstants.ScreenWidth * 9 ) / 16) + 5
            self.heightTbl.constant = cellHeight * CGFloat(arrSimiler.count)
        }
    }
    
    func addSwipe() {
        let directions: [UISwipeGestureRecognizerDirection] = [.right, .left, .up, .down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
            gesture.direction = direction
            self.view.addGestureRecognizer(gesture)
        }
    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        print(sender.direction)
        if sender.direction == .left {
            imageCount +=  1
        } else if sender.direction == .right {
            imageCount -= 1
        }
        swipeImage()
    }
    
    func swipeImage() {
        timerCount = 0
        imageCount = (imageCount < 0 ? (arrOnlyImages.count - 1) : (imageCount % arrOnlyImages.count) )
        UIView.transition(with: imgBg, duration: 0.75, options: .transitionCrossDissolve, animations: { self.imgBg.image = self.arrOnlyImages[self.imageCount] }, completion: nil)
    }
    
    //MARK: WS_ADD_RECENTLY
    func wsAddRecently() {
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["property_id"] = propertyId
        params["timezone"] = PredefinedConstants.localTimeZoneName
        
        let api = api_add_recently_view_property
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    //MARK: WS_PROPERTY_DETAIL
    func wsPropertyDetail() {
        
        let params = NSMutableDictionary()
        params["property_id"] = propertyId
        params["user_id"] = kAppDelegate.userId
        
        showHUD("Loading...".getLoclized())
        let api = api_listing_details
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            if result.count > 0 {
                                self.dictDetail = result.object(at: 0) as! NSDictionary
                                self.setDetails()
                            }
                        }
                        self.wsSoldHistory()
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    //MARK: WS_SOLD_HISTORY
    func wsSoldHistory() {
        let params = NSMutableDictionary()
        params["property_id"] = propertyId
        
        showHUD("Loading...".getLoclized())
        let api = api_get_property_sold_history_list
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    self.arrPriceHistory.removeAllObjects()
                    
                    let mdict = NSMutableDictionary()
                    mdict["sold_date"] = string(self.dictDetail, "created_date")
                    mdict["sold_status"] = string(self.dictDetail, "sale_type").capitalized.uppercased()
                    mdict["sold_price"] = string(self.dictDetail, "property_price")
                    self.arrPriceHistory.add(mdict)
                    
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            for i in 0..<result.count {
                                let dict = result.object(at: i) as! NSDictionary
                                self.arrPriceHistory.add(dict)
                            }
                        }
                    }
                    self.heightPriceHistory.constant = CGFloat((34 * self.arrPriceHistory.count) + 96)
                    self.tblPriceHistory.reloadData()
                }
            } else {
                self.removePriceHistory()
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    func removePriceHistory() {
        self.vwPriceHistory.isHidden = true
        self.heightPriceHistory.constant = 0.0
    }
    
    
    //MARK: WS_NOTIFY_ME
    func wsNotifyMe() {
        
        let params = NSMutableDictionary()
        params["property_id"] = propertyId
        params["user_id"] = kAppDelegate.userId
        
        showHUD("Loading...".getLoclized())
        let api = api_add_remove_notify
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if string(json, "result") != "Add successfull" {
                            self.lblNotifyMe.text = "Notify me for latest updates".getLoclized()
                        } else {
                            self.lblNotifyMe.text = "Turn off notifications".getLoclized()
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    
    //MARK: SCROLLVIEW DELEGATE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == scView {
            DispatchQueue.main.async {
                self.btnScHeader.backgroundColor = UIColor.hexColor(rgbValue: 0x553c37).withAlphaComponent(CGFloat(scrollView.contentOffset.y/(PredefinedConstants.ScreenHeight*0.8)))
                /*self.topBgImg.constant = 54.0
                self.bottomBgImg.constant = 0.0
                let offset = self.topBgImg.constant + -(scrollView.contentOffset.y/10.0)
                self.topBgImg.constant = offset
                if offset < 0 {
                    self.bottomBgImg.constant = offset
                }
                self.updateViewConstraints()*/
            }
        }
    }
    
    func downloadImage(_ imgUrl: URL) {
        
        SDWebImageManager.shared().imageDownloader?.downloadImage(with: imgUrl, options: .continueInBackground, progress: nil, completed: {(image:UIImage?, data:Data?, error:Error?, finished:Bool) in
            if image != nil {
                self.arrOnlyImages.append(image!)
                self.addSwipe()
                if self.arrOnlyImages.count == 2 {
                    self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.manageTimer), userInfo: nil, repeats: true)
                }
            }
        })
    }
    
    var timerCount = 0
    
    @objc func manageTimer() {
        timerCount += 1
        if timerCount == 3 {
            imageCount += 1
            swipeImage()
        }
    }
    
    //MARK: WEBVIEW DELEGATE
    func webViewDidStartLoad(_ webView: UIWebView) {
        showHUD("Lading...".getLoclized())
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        heightWebView.constant = self.wvDescription.scrollView.contentSize.height
        self.viewDidLayoutSubviews()
        hideHUD()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        hideHUD()
    }
    
    func actionMessage(index: IndexPath) {
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            let dict = arrSimiler.object(at: index.row) as! NSDictionary
            print("dict-\(dict)-")
            if let dictAgent = dict.object(forKey: "agent_details") as? NSDictionary {
                
                if string(dictAgent, "email") == string(kAppDelegate.userInfo, "email") {
                    Http.alert(appName, AlertMsg.cantChat)
                } else {
                    kAppDelegate.propertyChatID = propertyId
                    showHUD("")
                    if let id = Auth.auth().currentUser?.uid {
                        User.downloadAllUsers(exceptID: id, completion: {(user) in
                            DispatchQueue.main.async {
                                self.items.append(user)
                                for item in self.items {
                                    if string(dictAgent, "email") == item.email {
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! ChatVC
                                        vc.strNumber = self.tfAgentNumber.text!
                                        vc.currentUser = item
                                        vc.dictAgent = self.dictAgent
                                        self.navigationController?.pushViewController(vc, animated: true)
                                        self.navigationController?.setNavigationBarHidden(false, animated: true)
                                        break
                                    }
                                }
                                self.hideHUD()
                            }
                        })
                    }
                }
            }
        }
    }
    
    var selectedPropertyId = ""
    func actionFavorite(index: IndexPath) {
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            let dict = arrSimiler.object(at: index.row) as! NSDictionary
            selectedPropertyId = string(dict, "id")
            wsAddToFavourite(index)
        }
    }
    
    func actionShare(index: IndexPath) {
        let activityController = UIActivityViewController(activityItems: [inviteText], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true, completion: nil)
    }
    
    func actionDetail(index: IndexPath) {
        let dict = arrSimiler.object(at: index.row) as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dict, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tableView == tblPriceHistory) ? self.arrPriceHistory.count : self.arrSimiler.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblPriceHistory {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PriceHistoryCell", for: indexPath) as! PriceHistoryCell
            let dict = arrPriceHistory.object(at: indexPath.row) as! NSDictionary
            cell.lblDate.text = string(dict, "sold_date")
            cell.lblStatus.text = string(dict, "sold_status")
            cell.lblPrice.text = "\(string(dict, "sold_price"))  XAF"
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTblCell1", for: indexPath) as! HomeTblCell1
            cell.delegate = self
            cell.index = indexPath
            let dict = arrSimiler.object(at: indexPath.row) as! NSDictionary
            cell.arrImages = (dict.object(forKey: "property_images") as? NSArray)!
            cell.dictDetail = dict
            cell.cvImages.reloadData()
            if string(dict, "like_status") == "unlike" {
                cell.lblFavorite.text = "Favorite"
            } else {
                cell.lblFavorite.text = "Unfavorite"
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrSimiler.object(at: indexPath.row) as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dict, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func alertZero() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav")
        kAppDelegate.window?.rootViewController = vc
    }
    
    override func alertOne() {
        /*if let tabBar: UITabBarController = kAppDelegate.window?.rootViewController as? TabBarScreen {
            tabBar.selectedIndex = 2
        }*/
    }
    
    //MARK: WS_ADD_TO_FAVOURITE
    func wsAddToFavourite(_ index: IndexPath) {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["property_id"] = selectedPropertyId
        
        showHUD("Loading...".getLoclized())
        let api = api_add_to_favourite
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            for i in 0..<self.arrSimiler.count {
                                let dict = self.arrSimiler.object(at: i) as! NSDictionary
                                if string(dict, "id") == self.selectedPropertyId {
                                    let mDict = dict.mutableCopy() as! NSMutableDictionary
                                    mDict["like_status"] = string(result, "like_status")
                                    self.arrSimiler.replaceObject(at: i, with: mDict)
                                }
                                
                                if i < self.arrSimiler.count {
                                    let dict1 = self.arrSimiler.object(at: i) as! NSDictionary
                                    if string(dict1, "id") == self.selectedPropertyId {
                                        let mDict1 = dict1.mutableCopy() as! NSMutableDictionary
                                        mDict1["like_status"] = string(result, "like_status")
                                        self.arrSimiler.replaceObject(at: i, with: mDict1)
                                    }
                                }
                            }
                            self.tblSimilerProperty.reloadRows(at: [index], with: .none)
                        }
                    } else {
                        //Http.alert(appName, string(json, "message"))
                        print("response-\(String(describing: response.result.error?.localizedDescription))-")
                        print("response-\(response.result.description)-")
                        print("response-\(response.result.debugDescription)-")
                    }
                }
            } else {
                Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    
}//Class End
