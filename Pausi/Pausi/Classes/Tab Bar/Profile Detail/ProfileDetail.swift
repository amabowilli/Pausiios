//
//  ProfileDetail.swift
//  Pausi
//
//  Created by mac on 12/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase

class ProfileDetail: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var switchSharePhoto: UISwitch!
    @IBOutlet weak var lblVersion: UILabel!
    
    //MARK: VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        //lblVersion.text = "App Version: \(String(describing: PredefinedConstants.appVersion!))"
        lblVersion.text = "\("App Version:".getLoclized()) \(String(describing: PredefinedConstants.appVersion!))"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCamera(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionUpdateInfo(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionZipCode(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateZipCode") as! UpdateZipCode
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionSwitch(_ sender: UISwitch) {
    }
    
    @IBAction func actionNotification(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationList") as! NotificationList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionAgent(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllServices") as! AllServices
        vc.isUpdate = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionHelp(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/help.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func actionAbout(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/about.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func actionFollow(_ sender: Any) {
        let instURL: NSURL = NSURL (string: "instagram://user?username=pausi.inc")! // Replace = Instagram by the your instagram user name
        let instWB: NSURL = NSURL (string: "https://www.instagram.com/pausi.inc/")! // Replace the link by your instagram weblink
        //
        
        if (UIApplication.shared.canOpenURL(instURL as URL)) {
            // Open Instagram application
            UIApplication.shared.openURL(instURL as URL)
        } else {
            // Open in Safari
            UIApplication.shared.openURL(instWB as URL)
        }
    }
    
    @IBAction func actionRate(_ sender: Any) {
        
        let urlStr = "itms-apps://itunes.apple.com/app/id\(appID)" // (Option 1) Open App Page
        //let urlStr = "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=\(appID)" // (Option 2) Open App Review Tab
        
        if let url = URL(string: urlStr), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    @IBAction func actionLogout(_ sender: Any) {
        kAppDelegate.logoutUser()
        /*kAppDelegate.userInfo.removeAllObjects()
        kAppDelegate.userId = ""
        kAppDelegate.userType = ""
        kAppDelegate.profileUrl = ""
        UserDefaults.standard.removeObject(forKey: keyUserInfo)
        UserDefaults.standard.synchronize()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NavLanding")
        kAppDelegate.window?.rootViewController = vc
        kAppDelegate.wsChangeStatus("Offline")
        User.logOutUser { (status) in
            if status == true {
                self.dismiss(animated: true, completion: nil)
            }
        }*/
    }
    
    @IBAction func actionPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/privacy_policy.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func actionTerms(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/terms_service.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    //MARK: FUNCTIONS
    func showDetails() {
        lblName.text = "\(string(kAppDelegate.userInfo, "first_name")) \(string(kAppDelegate.userInfo, "last_name"))"
        lblEmail.text = string(kAppDelegate.userInfo, "email")
        lblNumber.text = ((string(kAppDelegate.userInfo, "mobile") == "") ? "--" : string(kAppDelegate.userInfo, "mobile"))
    }
    
}//Class End
