//
//  UpdateZipCode.swift
//  Pausi
//
//  Created by mac on 24/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlacePicker

class UpdateZipCode: UIViewController, UITextFieldDelegate, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfZipCode: UITextField!
    
    //MARK: VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        tfZipCode.text = string(kAppDelegate.userInfo, "zipcode")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUpdate(_ sender: Any) {
        if tfZipCode.text?.count == 0 {
            Http.alert(appName, AlertMsg.blankZip)
        } else {
            wsUpdateZipcode()
        }
    }
    
    //MARK: FUNCTIONS
    func showAddressController() {
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        present(placePickerController, animated: true, completion: nil)
    }
    
    //MARK: WS_UPDATE_ZIPCODE
    func wsUpdateZipcode() {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["zipcode"] = tfZipCode.text!
        
        let api = api_update_zipcode
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                            let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                            UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                            Http.alert(appName, AlertMsg.updateSuccess)
                            DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                                _=self.navigationController?.popToRootViewController(animated: true)
                            })
                        }
                        Http.alert(appName, AlertMsg.updateSuccess)
                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                            _=self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showAddressController()
        print("Hello")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("Hiiiii")
        showAddressController()
        return false
    }
    
    //MARK: AUTOCOMPLETEDVIEW DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        tfZipCode.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}//Class End

