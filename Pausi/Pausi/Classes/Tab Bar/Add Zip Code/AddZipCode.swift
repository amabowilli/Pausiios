//
//  AddZipCode.swift
//  Pausi
//
//  Created by mac on 22/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire

class AddZipCode: UIViewController, UITextFieldDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var vw1: View!
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var vw2: View!
    @IBOutlet weak var tf2: UITextField!
    @IBOutlet weak var vw3: View!
    @IBOutlet weak var tf3: UITextField!
    @IBOutlet weak var vw4: View!
    @IBOutlet weak var tf4: UITextField!
    @IBOutlet weak var vw5: View!
    @IBOutlet weak var tf5: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    //MARK: VARIABLES
    var arrList = NSMutableArray()
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        wsGetWallpaper()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSave(_ sender: Any) {
    }
    
    @IBAction func tf1EditingChanged(_ sender: Any) {
        if tf1.text?.count == 0 {
            
        } else {
            tf2.becomeFirstResponder()
        }
    }
    
    @IBAction func tf2EditingChanged(_ sender: Any) {
        if tf2.text?.count == 0 {
            tf1.becomeFirstResponder()
        } else {
            tf3.becomeFirstResponder()
        }
    }
    
    @IBAction func tf3EditingChanged(_ sender: Any) {
        if tf3.text?.count == 0 {
            tf2.becomeFirstResponder()
        } else {
            tf4.becomeFirstResponder()
        }
    }
    
    @IBAction func tf4EditingChanged(_ sender: Any) {
        if tf4.text?.count == 0 {
            tf3.becomeFirstResponder()
        } else {
            tf5.becomeFirstResponder()
        }
    }
    
    @IBAction func tf5EditingChanged(_ sender: Any) {
        if tf5.text?.count == 0 {
            tf4.becomeFirstResponder()
        } else {
            
        }
    }
    
    //MARK: FUNCTIONS
    func borderColor(_ vw: View? = nil) {
        vw1.layer.borderColor = UIColor.lightGray.cgColor
        vw2.layer.borderColor = UIColor.lightGray.cgColor
        vw3.layer.borderColor = UIColor.lightGray.cgColor
        vw4.layer.borderColor = UIColor.lightGray.cgColor
        vw5.layer.borderColor = UIColor.lightGray.cgColor
        vw?.layer.borderColor = UIColor.red.cgColor
    }
    
    
    //MARK: WS_GET_WALLPAPER
    func wsGetWallpaper() {
        let params = NSMutableDictionary()
        
        let api = api_all_wallpaper_list
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrList = result.mutableCopy() as! NSMutableArray
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    //MARK: WS_SET_WALLPAPER
    func wsSetWallpaper(_ id: String) {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["wallpaper_id"] = id
        
        let api = api_set_wallpaper
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        Http.alert(appName, AlertMsg.updateSuccess)
                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                            _=self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    
    //MARK: TEXTFIELD DELEGATE
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = textField.text!.count + string.count - range.length
        return (length <= 1) ?  true : false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tf1 {
            borderColor(vw1)
        } else if textField == tf2 {
            borderColor(vw2)
        } else if textField == tf3 {
            borderColor(vw3)
        } else if textField == tf4 {
            borderColor(vw4)
        } else if textField == tf5 {
            borderColor(vw5)
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        borderColor()
        return true
    }
    
}//Class End








