//
//  ZipCode.swift
//  Pausi
//
//  Created by mac on 22/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire

class ZipCode: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var vwNoZipCode: UIView!
    
    //MARK: VARIABLES
    var arrList = NSMutableArray()
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        wsGetWallpaper()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddZipCode") as! AddZipCode
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: FUNCTIONS
    
    
    
    //MARK: WS_GET_WALLPAPER
    func wsGetWallpaper() {
        let params = NSMutableDictionary()
        
        let api = api_all_wallpaper_list
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrList = result.mutableCopy() as! NSMutableArray
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    //MARK: WS_SET_WALLPAPER
    func wsSetWallpaper(_ id: String) {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["wallpaper_id"] = id
        
        let api = api_set_wallpaper
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        Http.alert(appName, AlertMsg.updateSuccess)
                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                            _=self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
}//Class End
