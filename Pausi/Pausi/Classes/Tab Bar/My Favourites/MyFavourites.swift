//
//  MyFavourites.swift
//  Pausi
//
//  Created by mac on 19/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class MyFavourites: UIViewController , UITableViewDataSource, UITableViewDelegate, HomeTblCellDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var vwPopUp: UIView!
    @IBOutlet weak var btnFavourite: UIButton!
    
    //MARK: VARIABLES
    var arrAllList = NSMutableArray()
    var selectedPropertyId = ""
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        btnFavourite.setTitle("Unfavorite", for: .normal)
        vwPopUp.frame = self.view.frame
        vwPopUp.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        wsGetFavoriteProperty()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    func actionShowPopUp(index: IndexPath) {
        let dictFav = arrAllList.object(at: index.row) as! NSDictionary
        let dict = dictFav.object(forKey: "property_details") as! NSDictionary
        selectedPropertyId = string(dict, "id")
        self.view.addSubview(vwPopUp)
    }
    
    @IBAction func actionRemovePopUp(_ sender: Any) {
        vwPopUp.removeFromSuperview()
    }
    
    @IBAction func actionSendMessage(_ sender: Any) {
        vwPopUp.removeFromSuperview()
    }
    
    @IBAction func actionFavourite(_ sender: Any) {
        vwPopUp.removeFromSuperview()
        wsAddToFavourite()
    }
    
    @IBAction func actionShare(_ sender: Any) {
        vwPopUp.removeFromSuperview()
        let activityController = UIActivityViewController(activityItems: [inviteText], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true, completion: nil)
    }
    
    
    //MARK: FUNCTIONS

    //MARK: WS_GET_FAVOURITE_PROPERTY
    func wsGetFavoriteProperty() {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        
        showHUD("Loading...".getLoclized())
        let api = api_get_favourite_property_lists
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrAllList = result.mutableCopy() as! NSMutableArray
                            self.tblView.reloadData()
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    //MARK: WS_ADD_TO_FAVOURITE
    func wsAddToFavourite() {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["property_id"] = selectedPropertyId
        
        showHUD("Loading...".getLoclized())
        let api = api_add_to_favourite
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        
                        for i in 0..<self.arrAllList.count {
                            let dictFav = self.arrAllList.object(at: i) as! NSDictionary
                            let dict = dictFav.object(forKey: "property_details") as! NSDictionary
                            if string(dict, "id") == self.selectedPropertyId {
                                self.arrAllList.removeObject(at: i)
                                self.tblView.reloadData()
                                break
                            }
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    
    
    //MARK: UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAllList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTblCell", for: indexPath) as! HomeTblCell
        cell.delegate = self
        cell.index = indexPath
        let dictFav = arrAllList.object(at: indexPath.row) as! NSDictionary
        let dict = dictFav.object(forKey: "property_details") as! NSDictionary
        let arrProperty = dict.object(forKey: "property_images") as? NSArray
        var imageUrl = ""
        if arrProperty != nil && arrProperty!.count > 0 {
            let dict1 = arrProperty?.object(at: 0) as! NSDictionary
            imageUrl = string(dict1, "property_image")
        }
        cell.imgBg.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "bg"), options: SDWebImageOptions(rawValue: 1), completed: nil)
        cell.lblPrice.text = "\(string(dict, "property_price")) XAF"
        //cell.lblStatus.text = "For \(string(dict, "sale_type"))"
        cell.lblStatus.text = "\(string(dict, "status"))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictFav = arrAllList.object(at: indexPath.row) as! NSDictionary
        let dict = dictFav.object(forKey: "property_details") as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dict, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
}//Class End
