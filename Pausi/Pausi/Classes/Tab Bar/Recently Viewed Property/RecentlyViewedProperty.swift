//
//  RecentlyViewedProperty.swift
//  Pausi
//
//  Created by mac on 07/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Firebase

protocol PropertyImageDelegate {
    func actionGetProperty(dict: NSDictionary);
}

class RecentlyViewedProperty: UIViewController, UITableViewDelegate, UITableViewDataSource, NewsCellDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: VARIABLES
    var arrNews = NSMutableArray()
    var delegate: PropertyImageDelegate? = nil
    var items = [User]()
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        wsRecentViewedProperty()
        print("delegate-\(delegate)-")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCamera(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func actionMessage(index: IndexPath) {
        let dict = arrNews.object(at: index.row) as! NSDictionary
        let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
        print("dictProperty-\(dictProperty)-")
        if let dictAgent = dictProperty.object(forKey: "agent_details") as? NSDictionary {
            if string(dictAgent, "email") == string(kAppDelegate.userInfo, "email") {
                Http.alert(appName, AlertMsg.cantChat)
            } else {
                kAppDelegate.propertyChatID = string(dictProperty, "id")
                showHUD("")
                if let id = Auth.auth().currentUser?.uid {
                    User.downloadAllUsers(exceptID: id, completion: {(user) in
                        DispatchQueue.main.async {
                            self.items.append(user)
                            for item in self.items {
                                if string(dictAgent, "email") == item.email {
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! ChatVC
                                    vc.currentUser = item
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                                    break
                                }
                            }
                            self.hideHUD()
                        }
                    })
                }
            }
        }
    }
    
    func actionFav(index: IndexPath) {
        wsAddToFavourite(index)
    }
    
    func acitonShare(index: IndexPath) {
        let activityController = UIActivityViewController(activityItems: [inviteText], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true, completion: nil)
    }
    
    func actionDetail(index: IndexPath) {
        let dict = arrNews.object(at: index.row) as! NSDictionary
        
        if delegate != nil {
            let dict1: NSDictionary = ["image": kAppDelegate.propertyImage as! UIImage, "id": string(dict, "property_id")]
            kAppDelegate.propertyImage = nil
            delegate?.actionGetProperty(dict: dict1)
            _=self.navigationController?.popViewController(animated: true)
        } else {
            let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
            vc.propertyId = string(dictProperty, "id")
            self.navigationController?.pushViewController(vc, animated: true)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    func screenShotMethod(_ vw: UIView) -> UIImage {
        //Create the UIImage
        UIGraphicsBeginImageContext(vw.frame.size)
        vw.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
        //Save it to the camera roll
    }
    
    //MARK: FUNCTIONS
    
    
    //MARK: UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentTblCell", for: indexPath) as! RecentTblCell
        cell.delegate = self
        cell.index = indexPath
        let dict = self.arrNews.object(at: indexPath.row) as! NSDictionary
        
        let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
        if let arrImages = dictProperty.object(forKey: "property_images") as? NSArray {
            cell.arrImages = arrImages
            cell.dictDetail = dictProperty
            cell.dict = dict
            cell.cvSlider.reloadData()
        }
        
        if string(dict, "like_status") == "unlike" {
            cell.lblFavorite.text = "Favorite"
        } else {
            cell.lblFavorite.text = "Unfavorite"
        }
        UIView.animate(withDuration: 0.5, animations: {
            //cell.cvSlider.frame.origin  = CGPoint(x : 22 , y: 3)
            cell.cvSlider.frame.origin  = CGPoint(x : 0 , y: 0)
        })
        cell.isCellOpen = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (PredefinedConstants.ScreenWidth*9)/16
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrNews.object(at: indexPath.row) as! NSDictionary
        print("delegate-\(delegate)-")
        if delegate != nil {
            let dict1: NSDictionary = ["image": kAppDelegate.propertyImage, "id": string(dict, "property_id")]
            delegate?.actionGetProperty(dict: dict1)
            _=self.navigationController?.popViewController(animated: true)
        } else {
            let dict = self.arrNews.object(at: indexPath.row) as! NSDictionary
            let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
            vc.propertyId = string(dictProperty, "id")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        /*
         if cell.isCellOpen {
         } else {
         self.tblbanklist.reloadData()
         }
         */
    }
    
    //MARK: WS_RECENTVIEWED_PROPERTY
    func wsRecentViewedProperty() {
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        
        showHUD("Loading...".getLoclized())
        let api = api_get_recently_viewed_property_lists
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrNews = result.mutableCopy() as! NSMutableArray
                            self.tblView.reloadData()
                            self.tblView.contentOffset.y = 0
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    
    //MARK: WS_ADD_TO_FAVOURITE
    func wsAddToFavourite(_ index: IndexPath) {
        
        let dict = arrNews.object(at: index.row) as! NSDictionary
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["property_id"] = string(dict, "property_id")
        
        showHUD("Loading...".getLoclized())
        let api = api_add_to_favourite
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            let mDict1 = dict.mutableCopy() as! NSMutableDictionary
                            mDict1["like_status"] = string(result, "like_status")
                            self.arrNews.replaceObject(at: index.row, with: mDict1)
                            //let cell = tblView.dequeueReusableCell(withIdentifier: "NewsCell", for: index) as! NewsCell
                            self.tblView.reloadRows(at: [index], with: .none)
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
}//Class End
