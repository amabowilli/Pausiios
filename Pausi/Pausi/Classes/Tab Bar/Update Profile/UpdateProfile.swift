//
//  UpdateProfile.swift
//  Pausi
//
//  Created by mac on 12/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire

class UpdateProfile: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    //MARK: VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        showDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUpdate(_ sender: Any) {
        if let strMsg = checkValidation() {
            Http.alert(appName, strMsg)
        } else {
            wsUpdateProfile()
        }
    }
    
    @IBAction func actionUpdatePassword(_ sender: Any) {
        if let strMsg = validationPassword() {
            Http.alert(appName, strMsg)
        } else {
            wsChangePassword()
        }
    }
    
    @IBAction func actionPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/privacy_policy.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func actionTerms(_ sender: Any) {
        if let url = URL(string: "http://technorizen.com/WORKSPACE1/pausi/terms_service.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    //MARK: FUNCTIONS
    func showDetails() {
        tfFirstName.text = string(kAppDelegate.userInfo, "first_name")
        tfLastName.text = string(kAppDelegate.userInfo, "last_name")
        tfEmail.text = string(kAppDelegate.userInfo, "email")
        tfNumber.text = string(kAppDelegate.userInfo, "mobile")
    }
    
    func checkValidation() -> String? {
        if tfFirstName.text?.count == 0 {
            return AlertMsg.blankFirstName
        } else if tfLastName.text?.count == 0 {
            return AlertMsg.blankLastName
        } else if tfEmail.text?.count == 0 {
            return AlertMsg.blankEmail
        } else if !(tfEmail.text!.isEmail) {
            return AlertMsg.invalidEmail
        } else if tfNumber.text?.count == 0 {
            return AlertMsg.blankNumber
        }
        return nil
    }
    
    func validationPassword() -> String? {
        if tfPassword.text?.count == 0 {
            return AlertMsg.blankCrntPass
        } else if tfNewPassword.text?.count == 0 {
            return AlertMsg.blankNewPass
        } else if tfConfirmPassword.text?.count == 0 {
            return AlertMsg.blankConfPass
        } else if tfConfirmPassword.text! != tfNewPassword.text! {
            return AlertMsg.passMisMatchNew
        }
        return nil
    }
    
    //MARK: WS_UPDATE_PROFILE
    func wsUpdateProfile() {
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["first_name"] = tfFirstName.text!
        params["last_name"] = tfLastName.text!
        params["mobile"] = tfNumber.text!
        params["user_type"] = UT_USER
        params["broker_title"] = string(kAppDelegate.userInfo, "broker_title")
        params["offer"] = string(kAppDelegate.userInfo, "offer")
        params["close_deals"] = string(kAppDelegate.userInfo, "close_deals")
        params["agent_since"] = string(kAppDelegate.userInfo, "agent_since")
        
        showHUD("Loading...".getLoclized())
        let api = api_user_update
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                            let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                            UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                            
                            Http.alert(appName, AlertMsg.updateSuccess)
                            DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                                _=self.navigationController?.popViewController(animated: true)
                            })
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    //MARK: WS_CHANGE_PASSWORD
    func wsChangePassword() {
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["old_password"] = tfPassword.text!
        params["password"] = tfNewPassword.text!
        
        showHUD("Loading...".getLoclized())
        let api = api_change_password
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        Http.alert(appName, AlertMsg.updateSuccess)
                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                            _=self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    
}//Class End
