//
//  ProfileTab.swift
//  Pausi
//
//  Created by mac on 19/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

protocol checkVwController {
    func checkController()
}

class ProfileTab: UIViewController {
    
    //MARK: OUTLETS
    var delegate: checkVwController!
    
    //MARK: VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else if kAppDelegate.userType == UT_USER {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Profile") as! Profile
            self.navigationController?.pushViewController(vc, animated: false)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AgentProfile") as! AgentProfile
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    
    //MARK: FUNCTIONSif kAppDelegate.userId == ""
    override func alertZero() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav")
        kAppDelegate.window?.rootViewController = vc
    }
    
    override func alertOne() {
        if let tabBar: UITabBarController = kAppDelegate.window?.rootViewController as? TabBarScreen {
            tabBar.selectedIndex = 2
        }
    }
    
}//Class End
