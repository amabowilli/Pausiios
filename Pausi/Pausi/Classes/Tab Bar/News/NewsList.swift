//
//  NewsList.swift
//  Pausi
//
//  Created by mac on 26/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Firebase

class NewsList: UIViewController, UITableViewDelegate, UITableViewDataSource, NewsCellDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: VARIABLES
    var arrNews = NSMutableArray()
    var items = [User]()
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        tblView.scrollsToTop = true
        self.tblView.contentOffset.y = 0
        self.tabBarController?.tabBar.isHidden = false
        if string(kAppDelegate.userInfo, "zipcode") == "" || string(kAppDelegate.userInfo, "zipcode") == "0" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "News") as! News
            self.navigationController?.pushViewController(vc, animated: false)
        } else {
            wsNewsList()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionCamera(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var isPushed = false
    func actionMessage(index: IndexPath) {
        isPushed = false
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            let dict = arrNews.object(at: index.row) as! NSDictionary
            let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
            print("dictProperty-\(dictProperty)-")
            if let dictAgent = dictProperty.object(forKey: "agent_details") as? NSDictionary {
                
                if string(dictAgent, "email") == string(kAppDelegate.userInfo, "email") {
                    Http.alert(appName, AlertMsg.cantChat)
                } else {
                    kAppDelegate.propertyChatID = string(dictProperty, "id")
                    showHUD("")
                    if let id = Auth.auth().currentUser?.uid {
                        User.downloadAllUsers(exceptID: id, completion: {(user) in
                            DispatchQueue.main.async {
                                self.items.append(user)
                                for item in self.items {
                                    if string(dictAgent, "email") == item.email {
                                        if !self.isPushed {
                                            self.isPushed = true
                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! ChatVC
                                            vc.currentUser = item
                                            self.navigationController?.pushViewController(vc, animated: true)
                                            self.navigationController?.setNavigationBarHidden(false, animated: true)
                                        }
                                    }
                                }
                                self.hideHUD()
                            }
                        })
                    }
                }
            }
        }
    }
    
    func actionFav(index: IndexPath) {
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            wsAddToFavourite(index)
        }
    }
    
    func acitonShare(index: IndexPath) {
        let activityController = UIActivityViewController(activityItems: [inviteText], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true, completion: nil)
    }
    
    func actionDetail(index: IndexPath) {
        let dict = arrNews.object(at: index.row) as! NSDictionary
        let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dictProperty, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: FUNCTIONS
    override func alertZero() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav")
        kAppDelegate.window?.rootViewController = vc
    }
    
    //MARK: UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.delegate = self
        cell.index = indexPath
        let dict = self.arrNews.object(at: indexPath.row) as! NSDictionary
        
        let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
        if let arrImages = dictProperty.object(forKey: "property_images") as? NSArray {
            cell.arrImages = arrImages
            cell.dictDetail = dictProperty
            cell.dict = dict
            cell.cvSlider.reloadData()
        }
        /*
         if string(dict, "like_status") == "unlike" {
         
         } else {
         cell.btnFav.setImage(#imageLiteral(resourceName: "star_rate"), for: .normal)
         }
         */
        if string(dict, "like_status") == "unlike" {
            cell.lblFavorite.text = "Favorite"
            //cell.btnFav.setImage(#imageLiteral(resourceName: "star_rate"), for: .normal)
        } else {
            cell.lblFavorite.text = "Unfavorite"
            //cell.btnFav.setImage(#imageLiteral(resourceName: "star"), for: .normal)
        }
        UIView.animate(withDuration: 0.5, animations: {
            //cell.cvSlider.frame.origin  = CGPoint(x : 22 , y: 3)
            cell.cvSlider.frame.origin  = CGPoint(x : 0 , y: 0)
        })
        
        cell.isCellOpen = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (PredefinedConstants.ScreenWidth*9)/16
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrNews.object(at: indexPath.row) as! NSDictionary
        let dictProperty = dict.object(forKey: "property_details") as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dictProperty, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        /*
         if cell.isCellOpen {
         } else {
         self.tblbanklist.reloadData()
         }
         */
    }
    
    //MARK: WS_NEWS_LIST
    func wsNewsList() {
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        
        showHUD("Loading...".getLoclized())
        let api = api_news_lists
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrNews = result.mutableCopy() as! NSMutableArray
                            self.tblView.reloadData()
                            //self.tblView.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
                            self.tblView.contentOffset.y = 0
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    
    //MARK: WS_ADD_TO_FAVOURITE
    func wsAddToFavourite(_ index: IndexPath) {
        
        let dict = arrNews.object(at: index.row) as! NSDictionary
        
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["property_id"] = string(dict, "property_id")
        
        showHUD("Loading...".getLoclized())
        let api = api_add_to_favourite
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            let mDict1 = dict.mutableCopy() as! NSMutableDictionary
                            mDict1["like_status"] = string(result, "like_status")
                            self.arrNews.replaceObject(at: index.row, with: mDict1)
                            //let cell = tblView.dequeueReusableCell(withIdentifier: "NewsCell", for: index) as! NewsCell
                            self.tblView.reloadRows(at: [index], with: .none)
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
}//Class End
