//
//  News.swift
//  Pausi
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire


class News: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate {
    
    //MARK: OUTLETS
    
    //MARK: VARIABLES
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionCamera(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionInviteFriend(_ sender: Any) {
        let activityControl = UIActivityViewController(activityItems: [inviteText], applicationActivities: nil)
        activityControl.popoverPresentationController?.sourceView = self.view
        self.present(activityControl, animated: true, completion: nil)
    }
    
    @IBAction func actionUpload(_ sender: Any) {
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            openFileAttachment()
        }
    }
    
    @IBAction func actionUpdateZipCode(_ sender: Any) {
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            self.tabBarController?.tabBar.isHidden = true
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateZipCode") as! UpdateZipCode
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: FUNCTIONS
    override func alertZero() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav")
        kAppDelegate.window?.rootViewController = vc
    }
    
    
    func openFileAttachment() {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery".getLoclized(), style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel".getLoclized(), style: UIAlertActionStyle.cancel, handler: nil))
        if PredefinedConstants.deviceType == .pad {
            actionSheet.popoverPresentationController?.sourceView = self.view
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK: PICKERVIEW DELEGATE
    var imgProfilePicture:UIImage? = nil
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var img:UIImage? = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let iii = info[UIImagePickerControllerEditedImage] as? UIImage {
            img = iii
        }
        if (img != nil) {
            imgProfilePicture = img
            wsUpdateProfile()
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: WS_UPDATE_PROFILE
    func wsUpdateProfile() {
        
        showHUD("Loading...".getLoclized())
        let params = [
            "user_id": kAppDelegate.userId,
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if self.imgProfilePicture != nil {
                multipartFormData.append(UIImageJPEGRepresentation(self.imgProfilePicture!, 0.5)!, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: api_update_user_image)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("progress-\(progress)-")
                })
                
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        self.hideHUD()
                        print("url-\(api_update_user_image)-")
                        print("params-\(params)-")
                        
                        if let json = response.result.value as? NSDictionary {
                            print("responce-\(json)-")
                            
                            if number(json, "status").boolValue {
                                if let result = json.object(forKey: "result") as? NSDictionary {
                                    kAppDelegate.userInfo = result.mutableCopy() as! NSMutableDictionary
                                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: result)
                                    UserDefaults.standard.set(archiveData, forKey: keyUserInfo)
                                    Http.alert(appName, AlertMsg.updateSuccess)
                                }
                            } else {
                                Http.alert(appName, string(json, "result"))
                            }
                            
                        } else {
                            Http.alert(appName, response.error?.localizedDescription)
                        }
                    }
                }
            case .failure(let encodingError):
                //Http.alert(appName, encodingError.localizedDescription)
                print("responce-\(encodingError.localizedDescription)-")
            }
        }
    }
    
    
}//Class End
