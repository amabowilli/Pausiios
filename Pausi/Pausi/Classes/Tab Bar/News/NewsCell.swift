//
//  NewsCell.swift
//  Pausi
//
//  Created by mac on 25/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import SDWebImage
import QuartzCore

protocol NewsCellDelegate {
    func actionMessage(index: IndexPath)
    func actionFav(index: IndexPath);
    func acitonShare(index: IndexPath);
    func actionDetail(index: IndexPath);
}

class NewsCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblBeds: UILabel!
    @IBOutlet weak var lblBath: UILabel!
    @IBOutlet weak var cvSlider: UICollectionView!
    @IBOutlet weak var vwLeft: UIView!
    @IBOutlet weak var lblFavorite: UILabel!
    @IBOutlet weak var leadingCv: NSLayoutConstraint!
    @IBOutlet weak var lblSold: UILabel!
    
    var delegate: NewsCellDelegate!
    var index: IndexPath!
    var arrImages = NSArray()
    var dictDetail = NSDictionary()
    var dict = NSDictionary()
    
    var panPoint = CGPoint()
    var panGesture: UIPanGestureRecognizer!
    var panStartPoint = CGPoint()
    var isCellOpen = false
    var lastPositionOfView = CGFloat()
    var cvCurrentIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cvSlider.delegate = self
        cvSlider.dataSource = self
        
        isCellOpen = false
        panGesture = UIPanGestureRecognizer(target: self, action: (#selector(NewsCell.gestureFunction(_:))))
        panGesture.delegate = self
        cvSlider.addGestureRecognizer(panGesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func actionMsg(_ sender: Any) {
        delegate.actionMessage(index: index)
    }
    
    @IBAction func actionFav(_ sender: Any) {
        delegate.actionFav(index: index)
    }
    
    @IBAction func actionShare(_ sender: Any) {
        delegate.acitonShare(index: index)
    }
    
    
    @objc func share() {
        delegate.acitonShare(index: index)
    }
    
    @objc func fav() {
        delegate.actionFav(index: index)
    }
    
    @objc func moreInfo() {
        delegate.actionDetail(index: index)
    }
    
    //MARK: COLLECTIONVIIEW DELEGATE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2//(arrImages.count + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCvCell", for: indexPath) as! HomeCvCell
        cell.lblPrice1.isHidden = true
        cell.lblDetail.isHidden = true
        cell.btnMoreOption.isHidden = true
        cvCurrentIndex = indexPath.row
        //print("cvCurrentIndex-\(cvCurrentIndex)-")
        if indexPath.row < 1 { //if indexPath.row < arrImages.count {
            cell.lblPrice.text = "\(string(dictDetail, "property_price")) XAF"
            cell.lblStatus.text = "  \(string(dictDetail, "status"))  "
            cell.lblAddress.text = string(dictDetail, "address")
            cell.lblBath.text = "\(string(dictDetail, "baths"))\n\("Baths".getLoclized())"
            cell.lblBeds.text = "\(string(dictDetail, "beds"))\n\("Rooms".getLoclized())"
            if (cell.btnFav != nil) {
                cell.btnFav.isHidden = false
                cell.btnShare.isHidden = false
            }
        } else {
            cell.lblPrice.text = ""
            cell.lblStatus.text = ""
            cell.lblAddress.text = ""
            cell.lblBath.text = ""
            cell.lblBeds.text = ""
        }
        
        if (cell.btnFav != nil) {
            cell.btnFav.addTarget(self, action: #selector(fav), for: .touchUpInside)
            cell.btnShare.addTarget(self, action: #selector(share), for: .touchUpInside)
            cell.btnMoreOption.addTarget(self, action: #selector(moreInfo), for: .touchUpInside)
            if string(dict, "like_status") == "unlike" {
                cell.btnFav.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            } else {
                cell.btnFav.setImage(#imageLiteral(resourceName: "star_rate"), for: .normal)
            }
        }

        if (string(dictDetail, "status") == "SOLD") || (string(dictDetail, "status") == "NOT AVAILABLE") {
            cell.lblStatus.backgroundColor = UIColor.red
        } else {
            cell.lblStatus.backgroundColor = PredefinedConstants.greenColor()
        }
        
        if indexPath.row < 1 { //indexPath.row < arrImages.count {
            if indexPath.row < arrImages.count, let dict = arrImages.object(at: indexPath.row) as? NSDictionary {
                cell.imgView.sd_setImage(with: URL(string: string(dict, "property_image")), placeholderImage: #imageLiteral(resourceName: "bg"), options: SDWebImageOptions(rawValue: 1), completed: nil)
            } else {
                cell.imgView.image = #imageLiteral(resourceName: "bg")
            }
        } else {
            cell.imgView.image = nil
            cell.lblPrice1.isHidden = false
            cell.lblDetail.isHidden = false
            cell.btnMoreOption.isHidden = false
            cell.lblPrice1.text = "\(string(dictDetail, "property_price")) XAF"
            //cell.lblDetail.text = "FOR \(string(dictDetail, "sale_type").capitalized.uppercased())\n\n\(string(dictDetail, "address"))\n\(string(dictDetail, "beds")) Beds • \(string(dictDetail, "baths"))  Baths • \(string(dictDetail, "acre_area"))  Acres • \(string(dictDetail, "sq_feet"))  Sq Ft"
            cell.lblDetail.text = "\(string(dictDetail, "status"))\n\n\(string(dictDetail, "address"))\n\(string(dictDetail, "beds")) \("Rooms".getLoclized()) • \(string(dictDetail, "baths"))  \("Baths".getLoclized()) • \(string(dictDetail, "sq_feet"))  M2"
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: PredefinedConstants.ScreenWidth, height: (PredefinedConstants.ScreenWidth*9)/16)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //propertyImage
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCvCell", for: indexPath) as! HomeCvCell
        
        UIGraphicsBeginImageContextWithOptions(cell.contentView.bounds.size, false, 0);
        cell.contentView.drawHierarchy(in: cell.contentView.bounds, afterScreenUpdates: true)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        kAppDelegate.propertyImage = image//snapshot
        
        //kAppDelegate.propertyImage = cell.vwBg.takeScreenshot()//snapshot
        //kAppDelegate.propertyImage = cell.vwBg.snapshot
        delegate.actionDetail(index: index)
    }
    
    //MARK: UISCROLLVIEW DELEGATE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        velocityPoint.y = 8.0
        if cvSlider.contentOffset.x == 0 {
            cvCurrentIndex = 0
        } else {
            cvCurrentIndex = 1
        }
    }
    
    var velocityPoint = CGPoint(x: 0.0, y: 0.0)
    @objc func gestureFunction(_ recognizer: UIPanGestureRecognizer) {
        velocityPoint = recognizer.velocity(in: cvSlider)
        //print("recognizer.state-\(recognizer.state)-")
        switch recognizer.state {
        case UIGestureRecognizerState.began:
            //let tblView = (self.superview)?.superview as! UITableView
            //tblView.reloadData()
            if velocityPoint.y < 8 && velocityPoint.y > -8 {
                self.panPoint = recognizer.translation(in: cvSlider)
            }
            //print("Pan began at: \(panPoint)")
            break
            
        case UIGestureRecognizerState.changed:
            if velocityPoint.y < 8 && velocityPoint.y > -8 {
            let currentPoint = recognizer.translation(in: cvSlider)
            let deltaX = currentPoint.x - self.panPoint.x
            //print("Pan moved: \(deltaX)")
            //print("is cell Open: \(isCellOpen)")
            
            if isCellOpen {
                //perform no task.
            } else { //cell closed
                if deltaX > 0 { //swipe right
                    if cvCurrentIndex == 0 {
                        if deltaX <= vwLeft.frame.width {
                            cvSlider.frame.origin.x = deltaX
                            lastPositionOfView = cvSlider.frame.origin.x
                        }
                    }
                }
            }
            }
            break
            
        case UIGestureRecognizerState.ended:
            //print("Pan ended")
            if isCellOpen {
                let currentPoint = recognizer.translation(in: cvSlider)
                let deltaX = currentPoint.x - self.panPoint.x
                
                if deltaX < 0 { //trying to close
                    isCellOpen = false
                    UIView.animate(withDuration: 0.5) {
                        self.cvSlider.frame.origin.x = 0
                        self.lastPositionOfView = 0
                    }
                }
            } else {//cell is closed
                let currentPoint = recognizer.translation(in: cvSlider)
                let deltaX = currentPoint.x - self.panPoint.x
                
                if deltaX > 0 { //trying to open
                    if lastPositionOfView >= 8 { //sufficient to open
                        if cvCurrentIndex == 0 {
                            cvSlider.frame.origin.x = getPositionAfterSwipe()
                            isCellOpen = true
                        }
                    } else {
                        cvSlider.frame.origin.x = 0
                    }
                }
            }
            break
        default:
            break
        }
    }
    
    func getPositionAfterSwipe() -> CGFloat {
        return vwLeft.frame.size.width
    }
    
    func getMinimumSwipeValue() -> CGFloat {
        return -(vwLeft.frame.size.width)
    }
    
    //MARK: GESTURE DELEGATE.
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    
    
    
    
}
