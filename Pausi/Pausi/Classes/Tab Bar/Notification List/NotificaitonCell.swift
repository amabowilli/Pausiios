//
//  NotificaitonCell.swift
//  Pausi
//
//  Created by mac on 19/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol NotificationCellDelegate {
    func actionSeeDetail(index: IndexPath);
}

class NotificaitonCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgView: ImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var delegate: NotificationCellDelegate!
    var index: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func actionSee(_ sender: Any) {
        delegate.actionSeeDetail(index: index)
    }
}



class NotificaitonCell1: UITableViewCell {
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func actionSee(_ sender: Any) {
    }
}

