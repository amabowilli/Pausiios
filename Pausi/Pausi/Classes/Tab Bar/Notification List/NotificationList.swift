//
//  NotificationList.swift
//  Pausi
//
//  Created by mac on 19/01/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class NotificationList: UIViewController, NotificationCellDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: VARIABLES
    var arrList = NSMutableArray()
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        wsNotificationList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        let arr = self.navigationController?.viewControllers
        if arr?.count ?? 0 > 1 {
            _=self.navigationController?.popViewController(animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
            kAppDelegate.window?.rootViewController = vc
        }
    }
    
    func actionSeeDetail(index: IndexPath) {
        let dict = arrList.object(at: index.row) as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dict, "property_id")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: FUNCTIONS
    
    
    
    //MARK: WS_NOTIFICATION_LIST
    func wsNotificationList() {
        self.showHUD("Loading...".getLoclized())
        let params = NSMutableDictionary()
        params["r_user_id"] = kAppDelegate.userId

        let api = api_get_property_notification
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrList = result.mutableCopy() as! NSMutableArray
                            self.tblView.reloadData()
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
}//Class End

extension NotificationList: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dict = arrList.object(at: indexPath.row) as! NSDictionary
        if string(dict, "tips") != "" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificaitonCell1", for: indexPath) as! NotificaitonCell1
            cell.lblTime.text = string(dict, "date_time").converDate("yyyy-MM-dd HH:mm:ss", "hh:mm a, dd MMM yyyy")
            cell.lblName.text = string(dict, "tips")
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificaitonCell", for: indexPath) as! NotificaitonCell
            cell.delegate = self
            cell.index = indexPath
            cell.lblTime.text = string(dict, "date_time").converDate("yyyy-MM-dd HH:mm:ss", "hh:mm a, dd MMM yyyy")
            if let dictProperty = dict.object(forKey: "property_details") as? NSDictionary {
                if string(dict, "notification_key") == "Sold Property" {
                    cell.lblName.text = "\(string(dictProperty, "property_name")) property has been sold."
                    
                } else if string(dict, "notification_key") == "Add Property" {
                    cell.lblName.text = "New property added in your area."
                } else {
                    cell.lblName.text = "\(string(dictProperty, "property_name")) property details has been updated."
                }
                if let arrImage = dictProperty.object(forKey: "property_images") as? NSArray {
                    if arrImage.count > 0 {
                        if let dictImage = arrImage.object(at: 0) as? NSDictionary {
                            cell.imgView.sd_setImage(with: URL(string: string(dictImage, "property_image")), placeholderImage: #imageLiteral(resourceName: "profile"), options: SDWebImageOptions(rawValue: 1), completed: nil)
                        }
                    }
                }
                
            }
            
            return cell
        }
    }
}

extension NotificationList: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
