//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import UIKit
import Firebase

class Conversation {
    
    //MARK: Properties
    let user: User
    var lastMessage: Message
    let otherUserId : String
    let propertyId : String

    //MARK: Methods
    class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            var conversations = [Conversation]()
            Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childAdded, with: { (snapshot) in
                
                if snapshot.exists() {
                    print("snapshot.key-\(snapshot.key)-")
                    let propertyId = snapshot.key
                    
                    Database.database().reference().child("users").child(currentUserID).child("conversations").child(propertyId).observe(.childAdded, with: { (snapshot) in
                        
                        if snapshot.exists() {
                            print("snapshot.key-\(snapshot.key)-")
                            let fromID = snapshot.key
                            
                            let values = snapshot.value as! [String: Any]
                            let location = values["location"] as! String
                            
                            Database.database().reference().child("conversations").child(location).observe(.value, with: { (snapshot) in
                                if snapshot.exists() {
                                    
                                    var isAllMsgDeleted = true
                                    
                                    for snap in snapshot.children {
                                        let receivedMessage = (snap as! DataSnapshot).value as! [String: Any]
                                        let isDelete = receivedMessage["\(currentUserID)"] as? String ?? "0"
                                        if isDelete != "1" {
                                            isAllMsgDeleted = false
                                        }
                                    }
                                    
                                    if !isAllMsgDeleted {
                                        
                                        User.info(forUserID: fromID, completion: { (user) in
                                            
                                            let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, senderName: "", senderProfile: "", receiverName: "", receiverProfile: "", property_id: kAppDelegate.propertyChatID)
                                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage, otherUserId: fromID, propertyId: "")
                                            
                                            conversations.append(conversation)
                                            
                                            conversation.lastMessage.downloadLastMessage(forLocation: location, completion: {
                                                
                                                completion(conversations)
                                            })
                                        })
                                    }
                                }
                            })
                        }
                    })
                }
            })
        }
    }
    
    //MARK: Inits
    init(user: User, lastMessage: Message, otherUserId: String, propertyId: String) {
        self.user = user
        self.lastMessage = lastMessage
        self.otherUserId = otherUserId
        self.propertyId = propertyId
    }
}
