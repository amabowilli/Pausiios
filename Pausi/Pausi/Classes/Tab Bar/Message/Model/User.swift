//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import Firebase
import FirebaseDatabase

class User: NSObject {
    
    //MARK: Properties
    let name: String
    let email: String
    let id: String
    let token: String
    let profileUrl: String

    //MARK: Methods
    /*class func registerUser(withName: String, email: String, password: String, completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                //user?.sendEmailVerification(completion: nil)
                let values = ["name": withName, "email": email]
                Database.database().reference().child("users").child((user?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                    if errr == nil {
                        //let userInfo = ["email" : email, "password" : password]
                        //UserDefaults.standard.set(userInfo, forKey: "userInformation")
                        completion(true)
                        print("firebase registration success")
                    }
                })
            }
            else {
                completion(false)
            }
        })
    }*/
    
    class func registerUser(withName: String, email: String, password: String, completion: @escaping (Bool, Error?) -> Swift.Void) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                //user?.sendEmailVerification(completion: nil)
                let values = ["name": withName, "email": email, "token": kAppDelegate.pushToken, "profileUrl": ((kAppDelegate.profileUrl == "") ? "1" : kAppDelegate.profileUrl)]
                //let values = ["name": withName, "email": email, "profilePicLink": path!]
                Database.database().reference().child("users").child((user?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                    if errr == nil {
                        let userInfo = ["email" : email, "password" : password]
                        UserDefaults.standard.set(userInfo, forKey: "userInformation")
                        completion(true, nil)
                    } else {
                        print("errr-\(errr)-")
                    }
                })
            } else {
                print("error-\(error?.localizedDescription)-")
                completion(false, error)
            }
        })
    }
    
   class func loginUser(withEmail: String, password: String, completion: @escaping (Bool, Error?) -> Swift.Void) {
        Auth.auth().signIn(withEmail: withEmail, password: password, completion: { (user, error) in
            if error == nil {
                let userInfo = ["email": withEmail, "password": password]
                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                
                let values = ["token": kAppDelegate.pushToken, "profileUrl": ((kAppDelegate.profileUrl == "") ? "1" : kAppDelegate.profileUrl)]
        Database.database().reference().child("users").child((user?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                    if errr == nil {
                        completion(true, nil)
                    } else {
                        print("errr-\(errr)-")
            }
                })
                
                completion(true, nil)
            } else {
                completion(false, error)
            }
        })
    }
    
    class func logOutUser(completion: @escaping (Bool) -> Swift.Void) {
        let values = ["token": "123"]
        Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
            do {
                try Auth.auth().signOut()
                UserDefaults.standard.removeObject(forKey: "userInformation")
                completion(true)
            } catch _ {
                completion(false)
            }
        })
        
    }
    
   class func info(forUserID: String, completion: @escaping (User) -> Swift.Void) {
    print("forUserID-\(forUserID)-")
   
    /*Database.database().reference().child("users").child(forUserID).child("credentials").observe(.value) { (snapshot) in
        if let data = snapshot.value as? [String: String] {
            let name = data["name"]!
            let email = data["email"]!
            let token = data["token"]!
            let profileUrl = data["profileUrl"]!
            let user = User.init(name: name, email: email, id: forUserID, token: token, profileUrl: profileUrl)
            completion(user)
        }
    }*/
    Database.database().reference().child("users").child(forUserID).child("credentials").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [String: Any] {
                let name = data["name"] as! String
                let email = data["email"] as! String
                let token = data["token"] as! String
                let profileUrl = data["profileUrl"] as! String
                let user = User.init(name: name, email: email, id: forUserID, token: token, profileUrl: profileUrl)
                completion(user)
            }
        })
    }
    
    class func downloadAllUsers(exceptID: String, completion: @escaping (User) -> Swift.Void) {
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            let id = snapshot.key
            let data = snapshot.value as! [String: Any]
            print("data-\(data)-")
            let credentials = data["credentials"] as! [String: Any]
            if id != exceptID {
                let name = credentials["name"]! as! String
                let email = credentials["email"]!  as! String
                let token = credentials["token"]!  as! String
                let profileUrl = credentials["profileUrl"]!  as! String
                let user = User.init(name: name, email: email, id: id, token: token, profileUrl: profileUrl)
                completion(user)
            }
        })
    }
    
    class func checkUserVerification(completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().currentUser?.reload(completion: { (_) in
            let status = (Auth.auth().currentUser?.isEmailVerified)!
            completion(status)
        })
    }

    
    //MARK: Inits
    init(name: String, email: String, id: String, token: String, profileUrl: String) {
        self.name = name
        self.email = email
        self.id = id
        self.token = token
        self.profileUrl = profileUrl
    }
}

