//
//  ChooseWallpaper.swift
//  Pausi
//
//  Created by mac on 18/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class ChooseWallpaper: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: VARIABLES
    var arrList = NSMutableArray()
    
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        wsGetWallpaper()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: FUNCTIONS
    
    
    
    //MARK: WS_GET_WALLPAPER
    func wsGetWallpaper() {
        let params = NSMutableDictionary()
        
        let api = api_all_wallpaper_list
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrList = result.mutableCopy() as! NSMutableArray
                            self.tblView.reloadData()
                        }
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    //MARK: WS_SET_WALLPAPER
    func wsSetWallpaper(_ id: String) {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["wallpaper_id"] = id

        let api = api_set_wallpaper
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        Http.alert(appName, AlertMsg.updateSuccess)
                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                            _=self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
}//Class End

extension ChooseWallpaper: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WallpaperCell", for: indexPath) as! WallpaperCell
        let dict = arrList.object(at: indexPath.row) as! NSDictionary
        cell.imgView.sd_setImage(with: URL(string: string(dict, "wallpaper_image")), placeholderImage: nil, options: SDWebImageOptions(rawValue: 1), completed: nil)
        return cell
    }
}

extension ChooseWallpaper: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrList.object(at: indexPath.row) as! NSDictionary
        wsSetWallpaper(string(dict, "id"))
    }
}

