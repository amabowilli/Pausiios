//
//  HomeTblCell1.swift
//  Pausi
//
//  Created by mac on 31/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import SDWebImage

protocol HomeTblCell1Delegate {
    func actionMessage(index: IndexPath)
    func actionFavorite(index: IndexPath)
    func actionShare(index: IndexPath)
    func actionDetail(index: IndexPath)
}

class HomeTblCell1: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var vwLeft: UIView!
    @IBOutlet weak var cvImages: UICollectionView!
    @IBOutlet weak var leadingCv: NSLayoutConstraint!
    @IBOutlet weak var lblFavorite: UILabel!
    
    var arrImages = NSArray()
    var dictDetail = NSDictionary()
    var index: IndexPath!
    var delegate: HomeTblCell1Delegate!
    
    var panPoint = CGPoint()
    var panGesture: UIPanGestureRecognizer!
    var panStartPoint = CGPoint()
    var isCellOpen = false
    var lastPositionOfView = CGFloat()
    var cvCurrentIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cvImages.delegate = self
        cvImages.dataSource = self
        
        isCellOpen = false
        panGesture = UIPanGestureRecognizer(target: self, action: (#selector(NewsCell.gestureFunction(_:))))
        panGesture.delegate = self
        cvImages.addGestureRecognizer(panGesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func actionMsg(_ sender: Any) {
        delegate.actionMessage(index: index)
    }
    
    @IBAction func actionFavorite(_ sender: Any) {
        delegate.actionFavorite(index: index)
    }
    
    @IBAction func actionShare(_ sender: Any) {
        delegate.actionShare(index: index)
    }
    
    //MARK: COLLECTIONVIIEW DELEGATE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (arrImages.count + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCvCell", for: indexPath) as! HomeCvCell
        cell.lblPrice1.isHidden = true
        cell.lblDetail.isHidden = true
        cell.btnMoreOption.isHidden = true
    
        if indexPath.row == 0 {
            cell.lblPrice.text = "\(string(dictDetail, "property_price")) XAF"
            //cell.lblStatus.text = "  For \(string(dictDetail, "sale_type"))  "
            cell.lblStatus.text = "  \(string(dictDetail, "status"))  "
        } else {
            cell.lblPrice.text = ""
            cell.lblStatus.text = ""
        }
        
        if indexPath.row < arrImages.count {
            let dict = arrImages.object(at: indexPath.row) as! NSDictionary
            cell.imgView.sd_setImage(with: URL(string: string(dict, "property_image")), placeholderImage: #imageLiteral(resourceName: "bg"), options: SDWebImageOptions(rawValue: 1), completed: nil)
        } else {
            //cell.imgView.backgroundColor = UIColor.green
            cell.imgView.image = nil
            cell.lblPrice1.isHidden = false
            cell.lblDetail.isHidden = false
            cell.btnMoreOption.isHidden = false
            cell.btnMoreOption.addTarget(self, action: #selector(self.showDetail), for: .touchUpInside)
            cell.lblPrice1.text = "\(string(dictDetail, "property_price")) XAF"
            //cell.lblDetail.text = "FOR \(string(dictDetail, "sale_type").capitalized.uppercased())\n\n\(string(dictDetail, "address"))\n\(string(dictDetail, "beds")) Beds • \(string(dictDetail, "baths"))  Baths • \(string(dictDetail, "acre_area"))  Acres • \(string(dictDetail, "sq_feet"))  Sq Ft"
            cell.lblDetail.text = "\(string(dictDetail, "status"))\n\n\(string(dictDetail, "address"))\n\(string(dictDetail, "beds")) \("Rooms".getLoclized()) • \(string(dictDetail, "baths"))  \("Baths".getLoclized()) • \(string(dictDetail, "sq_feet"))  M2"
        }
        return cell
    }
    
    @objc func showDetail() {
        delegate.actionDetail(index: index)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: PredefinedConstants.ScreenWidth, height: (PredefinedConstants.ScreenWidth*9)/16)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.actionDetail(index: index)
    }
    
    //MARK: UISCROLLVIEW DELEGATE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        velocityPoint.y = 8.0
        if cvImages.contentOffset.x == 0 {
            cvCurrentIndex = 0
        } else {
            cvCurrentIndex = 1
        }
    }
    
    var velocityPoint = CGPoint(x: 0.0, y: 0.0)
    @objc func gestureFunction(_ recognizer: UIPanGestureRecognizer) {
        velocityPoint = recognizer.velocity(in: cvImages)
        print("velocityPoint.y_\(velocityPoint.y)_")
        
            //print("recognizer.state-\(recognizer.state)-")
            switch recognizer.state {
            case UIGestureRecognizerState.began:
                //let tblView = (self.superview)?.superview as! UITableView
                //tblView.reloadData()
                if velocityPoint.y < 8 && velocityPoint.y > -8 {
                self.panPoint = recognizer.translation(in: cvImages)
                }
                //print("Pan began at: \(panPoint)")
                break
                
            case UIGestureRecognizerState.changed:
                if velocityPoint.y < 8 && velocityPoint.y > -8 {
                let currentPoint = recognizer.translation(in: cvImages)
                let deltaX = currentPoint.x - self.panPoint.x
                //print("Pan moved: \(deltaX)")
                //print("is cell Open: \(isCellOpen)")
                
                if isCellOpen {
                    //perform no task.
                } else { //cell closed
                    if deltaX > 0 { //swipe right
                        if cvCurrentIndex == 0 {
                            if deltaX <= vwLeft.frame.width {
                                cvImages.frame.origin.x = deltaX
                                lastPositionOfView = cvImages.frame.origin.x
                            }
                        }
                    }
                }
                }
                break
                
            case UIGestureRecognizerState.ended:
                //print("Pan ended")
                if isCellOpen {
                    let currentPoint = recognizer.translation(in: cvImages)
                    let deltaX = currentPoint.x - self.panPoint.x
                    
                    if deltaX < 0 { //trying to close
                        isCellOpen = false
                        UIView.animate(withDuration: 0.5) {
                            self.cvImages.frame.origin.x = 0
                            self.lastPositionOfView = 0
                        }
                    }
                } else {//cell is closed
                    let currentPoint = recognizer.translation(in: cvImages)
                    let deltaX = currentPoint.x - self.panPoint.x
                    
                    if deltaX > 0 { //trying to open
                        //if lastPositionOfView + getMinimumSwipeValue() >= 50 { //sufficient to open
                        if lastPositionOfView >= 8 { //sufficient to open
                            if cvCurrentIndex == 0 {
                                cvImages.frame.origin.x = getPositionAfterSwipe()
                                isCellOpen = true
                            }
                        } else {
                            cvImages.frame.origin.x = 0
                        }
                    }
                }
                break
            default:
                break
            }
    }
    
    func getPositionAfterSwipe() -> CGFloat {
        return vwLeft.frame.size.width
    }
    
    func getMinimumSwipeValue() -> CGFloat {
        return -(vwLeft.frame.size.width)
    }
    
    //MARK: GESTURE DELEGATE.
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
