//
//  Home.swift
//  Pausi
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlacePicker
import Alamofire
import SDWebImage
import Firebase
import GoogleMobileAds

class Home: UIViewController, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, HomeTblCellDelegate, HomeTblCell1Delegate, FilterDelegate, GADInterstitialDelegate {
    
    //MARK: OUTLETS
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var vwZoomOptions: UIView!
    @IBOutlet weak var scView: UIScrollView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnScrollUpper: UIButton!
    @IBOutlet weak var heightScrollUpper: NSLayoutConstraint!
    @IBOutlet weak var topMapView: NSLayoutConstraint!
    @IBOutlet weak var heightMap: NSLayoutConstraint!
    @IBOutlet weak var heightTbl: NSLayoutConstraint!
    @IBOutlet weak var topScroll: NSLayoutConstraint!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var stackIcons: UIStackView!
    @IBOutlet weak var vwBtnLocation: UIView!
    @IBOutlet weak var vwBtnOption: UIView!
    @IBOutlet weak var heightBtnOption: NSLayoutConstraint!
    @IBOutlet var vwPopUp: UIView!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var stackIconsHoriz: UIStackView!
    @IBOutlet weak var btnSchool: UIButton!
    @IBOutlet weak var btnHomeLocation: UIButton!
    @IBOutlet weak var btnSatellite: UIButton!
    @IBOutlet weak var vwCancelDrawing: View!{
        didSet{
            vwCancelDrawing.isHidden = true
        }
    }
    
    //MARK: VARIABLES
    var timer = Timer()
    var zoom: Float = 15.0
    var arrAllList = NSMutableArray()
    var arrMarkers = [GMSMarker]()
    var arrShownList = NSMutableArray()
    var isShownList = false
    var isZoomByBtn = false
    var imgOptionHeight: CGFloat = 0.0
    var selectedPropertyId = ""
    var type = "ALL"
    var isSketch = false
    var arrMapCircleCordinate = [CLLocationCoordinate2D]()
    var items = [User]()
    var interstitial: GADInterstitial!
    
    lazy var canvasView:CanvasView = {
        var overlayView = CanvasView(frame: self.mapView.frame)
        overlayView.isUserInteractionEnabled = true
        overlayView.delegate = self
        return overlayView
        
    }()
    var isDrawingModeEnabled = false
    var coordinates = [CLLocationCoordinate2D]()
    var userDrawablePolygons = [GMSPolygon]()
    var polygonDeleteMarkers = [DeleteMarker]()
    
    //MARK: FILTER VARIABLES
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        //wsGetProperty()
        setFrameAndConstant()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        vwPopUp.frame = self.view.frame
        stackIconsHoriz.isHidden = true
        vwPopUp.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(setMapContant), userInfo: nil, repeats: true)
        mapView.camera = GMSCameraPosition.camera(withLatitude: kAppDelegate.currentLocation.coordinate.latitude, longitude: kAppDelegate.currentLocation.coordinate.longitude, zoom: zoom)
        if kAppDelegate.userId != "" {
            checkForUpdateProfile()
        }
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-9304311353435424/5012044918") //Live
        //interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910") //Testing
        let request = GADRequest()
        interstitial.load(request)
        interstitial.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.scView.contentOffset.y = 0.0
        self.tabBarController?.tabBar.isHidden = false
        wsGetProperty()
    }
   
    override func viewDidAppear(_ animated: Bool) {
        imgOptionHeight = heightBtnOption.constant
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionSearch(_ sender: Any) {
        showAddressController()
        //print("Auth.auth().currentUser?.uid-\(Auth.auth().currentUser?.uid)-")
    }
    
    @IBAction func actionCamera(_ sender: Any) {
    }
    
    @IBAction func actionFilter(_ sender: Any) {
        //swiftHttpPostRequest()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Filter") as! Filter
         vc.delegate = self
        if type == "FILTER" {
            vc.saleType = self.saleType
            vc.minPrice = self.minPrice
            vc.maxPrice = self.maxPrice
            vc.minBed = self.minBed
            vc.maxBed = self.maxBed
            vc.minBath = self.minBath
            vc.maxBath = self.maxBath
            vc.propertyType = self.propertyType
            vc.sortBy = self.sort
            vc.status = self.status
            vc.date = self.date_by
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func actionZoomOut(_ sender: Any) {
        zoomMap(-1.0)
    }
    
    @IBAction func actionZoomIn(_ sender: Any) {
        zoomMap(1.0)
    }
    
    @IBAction func actionSearchIcons(_ sender: Any) {
        showAddressController()
    }
    
    @IBAction func actionScrollUpper(_ sender: Any) {
        UIView.animate(withDuration: 1) {
            self.topScroll.constant = PredefinedConstants.ScreenHeight
            self.frameMapForFullScreen()
        }
    }
    
    @IBAction func actionCurrentLocation(_ sender: Any) {
        if (mapView.myLocation != nil) {
            mapView.animate(toLocation: (mapView.myLocation?.coordinate)!)
        }
    }
    
    @IBAction func actionCancelDrawing(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "coordinates")
        arrMapCircleCordinate = []
        coordinates = []
        isDrawingModeEnabled = false
        let _ =  userDrawablePolygons.map{ $0.map = nil }
        let _ = polygonDeleteMarkers.map{ $0.map = nil}
        polygonDeleteMarkers.removeAll()
        userDrawablePolygons.removeAll()
        vwCancelDrawing.isHidden = true
        addPinInMap()
    }
    
    var isSchool = false
    @IBAction func actionSchool(_ sender: Any) {
        isShow = false
        stackIconsHoriz.isHidden = true
        isSchool = !isSchool
        if isSchool {
            btnSchool.setImage(#imageLiteral(resourceName: "home_school2"), for: .normal)
            //self.mapView.mapType =
            Http.alert("Schools Layer On".getLoclized(), "Move the map to\nexplore schools.".getLoclized())
        } else {
            btnSchool.setImage(#imageLiteral(resourceName: "home_school1"), for: .normal)
        }
    }
    
    @IBAction func actionDrawing(_ sender: Any) {
        stackIcons.isHidden = true
        vwCancelDrawing.isHidden = false
        stackIconsHoriz.isHidden = true
        isShow = false
        self.coordinates.removeAll()
        arrMapCircleCordinate = []
        self.view.addSubview(canvasView)
    }
    
    var isLocation = false
    @IBAction func actionZoomInOut(_ sender: Any) {
        isShow = false
        stackIconsHoriz.isHidden = true
        isLocation = !isLocation
        if isLocation {
            zoomMap(1.0)
            btnHomeLocation.setImage(#imageLiteral(resourceName: "home_location1"), for: .normal)
        } else {
            zoomMap(-1.0)
            btnHomeLocation.setImage(#imageLiteral(resourceName: "home_location"), for: .normal)
        }
    }
    
    var isSattelliteMode = false
    @IBAction func actionChangeMapMode(_ sender: Any) {
        isShow = false
        stackIconsHoriz.isHidden = true
        isSattelliteMode = !isSattelliteMode
        if isSattelliteMode {
            self.mapView.mapType = .satellite
            btnSatellite.setImage(#imageLiteral(resourceName: "home_satellite1"), for: .normal)
        } else {
            self.mapView.mapType = .normal
            btnSatellite.setImage(#imageLiteral(resourceName: "home_satellite"), for: .normal)
        }
    }
    
    @IBAction func actionShowZoomOption(_ sender: Any) {
        isShownList = false
        heightBtnOption.constant = 0.0
        heightMap.constant = self.view.frame.size.height/2
        stackIcons.isHidden = true
        btnLocation.isHidden = false
        showPropertyList()
    }
    
    var isShow = false
    
    @IBAction func actionShowMapOptions(_ sender: Any) {
        isShow = !isShow
        if isShow {
            stackIconsHoriz.isHidden = false
        } else {
            stackIconsHoriz.isHidden = true
        }
    }
    
    func actionShowPopUp(index: IndexPath) {
        /*let dict = arrShownList.object(at: index.row) as! NSDictionary
        selectedPropertyId = string(dict, "id")
        if string(dict, "like_status") == "unlike" {
            btnFavourite.setTitle("Favorite", for: .normal)
        } else {
            btnFavourite.setTitle("Unfavorite", for: .normal)
        }
        kAppDelegate.window?.addSubview(vwPopUp)*/
    }
    
    var isPushed = false
    
    func actionMessage(index: IndexPath) {
        isPushed = false
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            let dict = arrShownList.object(at: index.row) as! NSDictionary
            print("dict-\(dict)-")
            if let dictAgent = dict.object(forKey: "agent_details") as? NSDictionary {
                if string(dictAgent, "email") == string(kAppDelegate.userInfo, "email") {
                    Http.alert(appName, AlertMsg.cantChat)
                } else {
                    kAppDelegate.propertyChatID = string(dict, "id")
                    showHUD("")
                    if let id = Auth.auth().currentUser?.uid {
                        User.downloadAllUsers(exceptID: id, completion: {(user) in
                            DispatchQueue.main.async {
                                self.items.append(user)
                                for item in self.items {
                                    
                                    print("item-\(item.email)-")
                                    print("itememail-\(string(dictAgent, "email"))-")
                                    
                                    if string(dictAgent, "email") == item.email {
                                        if !self.isPushed {
                                            self.isPushed = true
                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! ChatVC
                                            vc.currentUser = item
                                            vc.dictAgent = dictAgent
                                            self.navigationController?.pushViewController(vc, animated: true)
                                            self.navigationController?.setNavigationBarHidden(false, animated: true)
                                        }
                                    }
                                }
                                self.hideHUD()
                            }
                        })
                    }
                }
            }
        }
    }
    
    func actionFavorite(index: IndexPath) {
        if kAppDelegate.userId == "" {
            Http.alert("Alert".getLoclized(), "You need to login first".getLoclized(), [self, "LOGIN".getLoclized(), "CANCEL".getLoclized()])
        } else {
            let dict = arrShownList.object(at: index.row) as! NSDictionary
            selectedPropertyId = string(dict, "id")
            wsAddToFavourite(index)
        }
    }
    
    func actionShare(index: IndexPath) {
        let activityController = UIActivityViewController(activityItems: [inviteText], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true, completion: nil)
    }
    
    func actionDetail(index: IndexPath) {
        let dict = arrShownList.object(at: index.row) as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dict, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func actionRemovePopUp(_ sender: Any) {
        vwPopUp.removeFromSuperview()
    }
    
    @IBAction func actionSendMessage(_ sender: Any) {
        vwPopUp.removeFromSuperview()
    }
    
    @IBAction func actionFavourite(_ sender: Any) {
        vwPopUp.removeFromSuperview()
        //wsAddToFavourite()
    }
    
    @IBAction func actionShare(_ sender: Any) {
        vwPopUp.removeFromSuperview()
    }
    
    //MARK: FUNCTIONS
    func showDrawingOnMap() {
        
        if let archiveData = UserDefaults.standard.object(forKey: "coordinates") as? Data {
            if let cordinate = NSKeyedUnarchiver.unarchiveObject(with: archiveData) as? NSArray {
                var arrCords = [CLLocationCoordinate2D]()
                for i in 0..<cordinate.count {
                    let dict = cordinate.object(at: i) as! NSDictionary
                    arrCords.append(CLLocationCoordinate2D(latitude: Double(truncating: number(dict, "lat")), longitude: Double(truncating: number(dict, "lon"))))
                }
                vwCancelDrawing.isHidden = false
                self.arrMapCircleCordinate = arrCords
                self.coordinates = arrCords
                createPolygonFromTheDrawablePoints()
            }
        }
    }
    
    func applyFilter(property_type: String, sale_type: String, min_price: String, max_price: String, min_bed: String, max_bed: String, min_bath: String, max_bath: String, status_by: String, date_by: String, sort_by: String, amenities: String) {
        type = "FILTER"
        self.propertyType = property_type
        self.saleType = sale_type
        self.minPrice = min_price
        self.maxPrice = max_price
        self.minBed = min_bed
        self.maxBed = max_bed
        self.minBath = min_bath
        self.maxBath = max_bath
        self.status = status_by
        self.date_by = date_by
        self.sort = sort_by
        self.amenities = amenities
        print("Hello Boty..\(type).")
    }
    
    func resetFilter() {
        type = "ALL"
        self.propertyType = ""
        self.saleType = ""
        self.minPrice = ""
        self.maxPrice = ""
        self.minBed = ""
        self.maxBed = ""
        self.minBath = ""
        self.maxBath = ""
        self.status = ""
        self.date_by = ""
        self.sort = ""
        self.amenities = ""
        print("Hello Boty..\(type).")
    }
    
    func createPolygonFromTheDrawablePoints(){
        let arrCordinate = NSMutableArray()
        for i in 0..<self.coordinates.count {
            let cords = self.coordinates[i]
            arrCordinate.add(["lat": cords.latitude, "lon": cords.longitude])
        }
        let archiveData = NSKeyedArchiver.archivedData(withRootObject: arrCordinate)
        UserDefaults.standard.set(archiveData, forKey: "coordinates")
        let numberOfPoints = self.coordinates.count
        //do not draw in mapview a single point
        if numberOfPoints > 2 { addPolyGonInMapView(drawableLoc: coordinates) }//neglects a single touch
        coordinates = [] //KUNDAN REMOVE CORDINATE
        self.canvasView.image = nil
        self.canvasView.removeFromSuperview()
    }
    
    
    var newpolygon = GMSPolygon()
    
    func  addPolyGonInMapView( drawableLoc:[CLLocationCoordinate2D]){
        
        isDrawingModeEnabled = true
        let path = GMSMutablePath()
        for loc in drawableLoc{
            path.add(loc)
        }
        newpolygon = GMSPolygon()
        newpolygon = GMSPolygon(path: path)
        newpolygon.strokeWidth = 3
        newpolygon.strokeColor = PredefinedConstants.appColor()
        newpolygon.fillColor = PredefinedConstants.appColor().withAlphaComponent(0.5)
        newpolygon.map = mapView
        if vwCancelDrawing.isHidden == true{ vwCancelDrawing.isHidden = false }
        userDrawablePolygons.append(newpolygon)
        addPolygonDeleteAnnotation(endCoordinate: drawableLoc.last!,polygon: newpolygon)
    }
    
    func addPolygonDeleteAnnotation(endCoordinate location:CLLocationCoordinate2D,polygon:GMSPolygon){
        
        /*let marker = DeleteMarker(location: location,polygon: polygon)
        let deletePolygonView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        deletePolygonView.layer.cornerRadius = 15
        deletePolygonView.backgroundColor = UIColor.white
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        imageView.center = deletePolygonView.center
        imageView.image = UIImage(named: "delete")
        deletePolygonView.addSubview(imageView)
        marker.iconView = deletePolygonView
        marker.map = mapView
        polygonDeleteMarkers.append(marker)*/
    }
    
    func checkForUpdateProfile() {
        if kAppDelegate.userType == UT_USER {
            if string(kAppDelegate.userInfo, "mobile") == "" || string(kAppDelegate.userInfo, "mobile") == "0" {
                Http.alert(appName, AlertMsg.updateProfile, [self, "YES".getLoclized(), "Later".getLoclized()])
            }
        } else {
            if string(kAppDelegate.userInfo, "id_proof").range(of: ".png") == nil {
                Http.alert(appName, AlertMsg.updateProfile, [self, "YES".getLoclized(), "Later".getLoclized()])
            }
        }
    }
    
    /*
     
     */
    
    override func alertZero() {
        if kAppDelegate.userId == "" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav")
            kAppDelegate.window?.rootViewController = vc
        } else if kAppDelegate.userType == UT_USER {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AgentUpdateInfo") as! AgentUpdateInfo
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func alertOne() {
        /*if kAppDelegate.userId == "" {
            if let tabBar: UITabBarController = kAppDelegate.window?.rootViewController as? TabBarScreen {
                tabBar.selectedIndex = 2
            }
        }*/
    }
    
    func zoomMap(_ zoomBy: Float) {
        isZoomByBtn = true
        zoom += zoomBy
        self.mapView.animate(toZoom: zoom)
    }
    
    func showAddressController() {
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        present(placePickerController, animated: true, completion: nil)
    }
    
    func setFrameAndConstant() {
        heightBtnOption.constant = 0.0
        heightMap.constant = self.view.frame.size.height/2
        stackIcons.isHidden = true
        btnLocation.isHidden = false
        topScroll.constant = PredefinedConstants.ScreenHeight
        heightScrollUpper.constant = PredefinedConstants.ScreenHeight/2
        heightTbl.constant = self.tblView.contentSize.height
        scView.contentSize.height = self.tblView.frame.origin.y + self.heightTbl.constant
        updateViewConstraints()
    }
    
    @objc func setMapContant() {
        if kAppDelegate.currentLocation.coordinate.latitude != 0.0 && kAppDelegate.currentLocation.coordinate.longitude != 0.0 {
            mapView.camera = GMSCameraPosition.camera(withLatitude: kAppDelegate.currentLocation.coordinate.latitude, longitude: kAppDelegate.currentLocation.coordinate.longitude, zoom: 15)
            self.timer.invalidate()
        }
    }
    
    func addPinInMap() {
        arrMarkers.removeAll()
        self.mapView.clear()
        /*
         self.coordinates.removeAll()
         arrMapCircleCordinate = []
         self.view.addSubview(canvasView)
         */
        showDrawingOnMap()
        
        if arrMapCircleCordinate.count > 0 {
            for i in 0..<arrAllList.count {
                let dict = arrAllList.object(at: i) as! NSDictionary
                let position = CLLocationCoordinate2D(latitude: Double(truncating: number(dict, "lat")), longitude: Double(truncating: number(dict, "lon")))
                print("position-\(position)-")
                if isMarkerWithinDrawing(position: position) {
                    let marker = GMSMarker(position: position)
                    marker.icon = #imageLiteral(resourceName: "pin")
                    marker.userData = dict
                    marker.map = self.mapView
                    arrMarkers.append(marker)
                }
            }
        } else {
            for i in 0..<arrAllList.count {
                let dict = arrAllList.object(at: i) as! NSDictionary
                let position = CLLocationCoordinate2D(latitude: Double(truncating: number(dict, "lat")), longitude: Double(truncating: number(dict, "lon")))
                print("position-\(position)-")
                let marker = GMSMarker(position: position)
                marker.icon = #imageLiteral(resourceName: "pin")
                marker.userData = dict
                marker.map = self.mapView
                arrMarkers.append(marker)
            }
        }
        self.showPropertyList()
        /*if !self.isShownList {
            self.showPropertyList()
        }*/
    }
    
    var saleType = ""
    var minPrice = ""
    var maxPrice = ""
    var minBed = ""
    var maxBed = ""
    var minBath = ""
    var maxBath = ""
    var propertyType = ""
    var sort = ""
    var status = ""
    var date_by = ""
    var amenities = ""
    
    
    //MARK: WS_GET_PROPERTY
    func wsGetProperty() {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["type"] = type
        params["property_type"] = propertyType
        params["sale_type"] = saleType
        params["from_price"] = minPrice
        params["to_price"] = maxPrice
        params["from_bed"] = minBed
        params["to_bed"] = maxBed
        params["from_bath"] = minBath
        params["to_bath"] = maxBath
        params["sort_type"] = ""//sort
        params["date_type"] = date_by
        params["status"] = status
        params["property_amenities"] = amenities

        showHUD("Loading...".getLoclized())
        let api = api_all_listing_list
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        if let result = json.object(forKey: "result") as? NSArray {
                            self.arrAllList.removeAllObjects()
                            self.arrAllList = result.mutableCopy() as! NSMutableArray
                            self.addPinInMap()
                        }
                    } else {
                        self.arrAllList.removeAllObjects()
                        self.addPinInMap()
                        Http.alert(appName, string(json, "message"))
                    }
                }
            } else {
                //Http.alert(appName, response.error?.localizedDescription)
                print("response-\(String(describing: response.result.error?.localizedDescription))-")
                print("response-\(response.result.description)-")
                print("response-\(response.result.debugDescription)-")
            }
        }
    }
    
    //https://maps.googleapis.com/maps/api/place/textsearch/json?query=Mexican+Restaurant&sensor=true&location=22.7571,75.8822&radius=20&key=AIzaSyCo2eGa5b9mUSxWq9Ukr5hpe7TkhlA70GQ
    //https://maps.googleapis.com/maps/api/place/textsearch/json?query=Springfield%20Elementary&type=school&key=AIzaSyCo2eGa5b9mUSxWq9Ukr5hpe7TkhlA70GQ
    
    
    //MARK: WS_ADD_TO_FAVOURITE
    func wsAddToFavourite(_ index: IndexPath) {
        let params = NSMutableDictionary()
        params["user_id"] = kAppDelegate.userId
        params["property_id"] = selectedPropertyId

        showHUD("Loading...".getLoclized())
        let api = api_add_to_favourite
        Alamofire.request(api, method: .get, parameters: params as? [String: Any], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            self.hideHUD()
            print("url-\(api)-")
            print("params-\(params)-")
            if response.result.value != nil {
                if let json = response.result.value as? NSDictionary {
                    print("responce-\(json)-")
                    if number(json, "status").boolValue {
                        
                        if let result = json.object(forKey: "result") as? NSDictionary {
                            for i in 0..<self.arrAllList.count {
                                let dict = self.arrAllList.object(at: i) as! NSDictionary
                                if string(dict, "id") == self.selectedPropertyId {
                                    let mDict = dict.mutableCopy() as! NSMutableDictionary
                                    mDict["like_status"] = string(result, "like_status")
                                    self.arrAllList.replaceObject(at: i, with: mDict)
                                }
                                
                                if i < self.arrShownList.count {
                                    let dict1 = self.arrShownList.object(at: i) as! NSDictionary
                                    if string(dict1, "id") == self.selectedPropertyId {
                                        let mDict1 = dict1.mutableCopy() as! NSMutableDictionary
                                        mDict1["like_status"] = string(result, "like_status")
                                        self.arrShownList.replaceObject(at: i, with: mDict1)
                                    }
                                }
                            }
                            self.tblView.reloadRows(at: [index], with: .none)
                        }
                    } else {
                        //Http.alert(appName, string(json, "message"))
                        print("response-\(String(describing: response.result.error?.localizedDescription))-")
                        print("response-\(response.result.description)-")
                        print("response-\(response.result.debugDescription)-")
                    }
                }
            } else {
                Http.alert(appName, response.error?.localizedDescription)
            }
        }
    }
    
    //MARK: UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrShownList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTblCell1", for: indexPath) as! HomeTblCell1
        cell.delegate = self
        cell.index = indexPath
        let dict = arrShownList.object(at: indexPath.row) as! NSDictionary
        cell.arrImages = (dict.object(forKey: "property_images") as? NSArray)!
        cell.dictDetail = dict
        cell.cvImages.reloadData()
        if string(dict, "like_status") == "unlike" {
            cell.lblFavorite.text = "Favorite"
        } else {
            cell.lblFavorite.text = "Unfavorite"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrShownList.object(at: indexPath.row) as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
        vc.propertyId = string(dict, "id")
        self.navigationController?.pushViewController(vc, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: SCROLLVIEW DELEGATE
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == scView {
            DispatchQueue.main.async {
                self.topMapView.constant = 0.0
                let offset = self.topMapView.constant + -(scrollView.contentOffset.y/2.0)
                //if offset <= 0 {
                    self.topMapView.constant = offset
                //}
                self.updateViewConstraints()
            }
        }
    }
    
    //MARK: MAPVIEW DELEGATE
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        zoom = self.mapView.camera.zoom
        if !isShownList {
            showPropertyList()
        }
    }
    
    /*func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let dict = marker.userData as? NSDictionary {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PropertyDetail") as! PropertyDetail
            vc.propertyId = string(dict, "id")
            self.navigationController?.pushViewController(vc, animated: true)
            self.tabBarController?.tabBar.isHidden = true
        }
        return true
    }*/
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        //do not accept touch of current location marker
        if let deletemarker = marker as? DeleteMarker{
            
            if let index = userDrawablePolygons.index(of: deletemarker.drawPolygon){
                
                userDrawablePolygons.remove(at: index)
                
            }
            if let  indexToRemove =  polygonDeleteMarkers.index(of: deletemarker){
                
                polygonDeleteMarkers.remove(at: indexToRemove)
                
            }
            
            deletemarker.drawPolygon.map = nil
            deletemarker.map = nil
            //if userDrawablePolygons.count == 0{ cancelDrawingActn(nil) }
            return true
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        frameMapForFullScreen()
    }
    
    func frameMapForFullScreen() {
        isShownList = true
        self.topMapView.constant = 0.0
        self.heightMap.constant = PredefinedConstants.ScreenHeight - (self.tabBarController?.tabBar.frame.size.height)!
        stackIcons.isHidden = false
        btnLocation.isHidden = true
        self.heightBtnOption.constant = imgOptionHeight
    }
    
    func showPropertyList() {
        self.arrShownList.removeAllObjects()
        for marker in arrMarkers {
            if isMarkerWithinScreen(marker: marker) {
                if let dict = marker.userData as? NSDictionary {
                    self.arrShownList.add(dict)
                }
            }
        }
        
        self.tblView.reloadData()
        heightMap.constant = PredefinedConstants.ScreenHeight/2
        stackIcons.isHidden = true
        btnLocation.isHidden = false
        
        if self.arrShownList.count > 0 {
            isShownList = true
            vwZoomOptions.isHidden = true
            scView.bringSubview(toFront: self.view)
            let cellHeight = ((PredefinedConstants.ScreenWidth * 9 ) / 16) + 5
            let tableHeight = cellHeight * CGFloat(arrShownList.count)
            self.heightTbl.constant = tableHeight
            self.scView.contentSize.height = self.tblView.frame.origin.y + self.heightTbl.constant
            self.updateViewConstraints()
            UIView.animate(withDuration: 1) {
                self.topScroll.constant = 0.0
            }
        } else {
            vwZoomOptions.isHidden = false
            UIView.animate(withDuration: 1) {
                self.topScroll.constant = PredefinedConstants.ScreenHeight
            }
        }
    }
    
    func isMarkerWithinScreen(marker: GMSMarker) -> Bool {
        let region = self.mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        return bounds.contains(marker.position)
    }
    
    func isMarkerWithinDrawing(position: CLLocationCoordinate2D) -> Bool {
        if newpolygon.path != nil {
            if GMSGeometryContainsLocation(position, newpolygon.path!, true) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
    }
    
    //MARK: AUTOCOMPLETEDVIEW DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.mapView.animate(toLocation: place.coordinate)
        tfAddress.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            self.showPropertyList()
        })
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK: ADDMOB DELEGATE
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        if self.interstitial.isReady {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
    
    
}//Class End





//MARK: GET DRAWABLE COORDINATES
extension Home:NotifyTouchEvents{
    
    func touchBegan(touch:UITouch){
        let location = touch.location(in: self.mapView)
        let coordinate = self.mapView.projection.coordinate(for: location)
        self.coordinates.append(coordinate)
        
    }
    
    func touchMoved(touch:UITouch){
        let location = touch.location(in: self.mapView)
        let coordinate = self.mapView.projection.coordinate(for: location)
        self.coordinates.append(coordinate)
    }
    
    func touchEnded(touch:UITouch){
        stackIcons.isHidden = false
        let location = touch.location(in: self.mapView)
        let coordinate = self.mapView.projection.coordinate(for: location)
        self.coordinates.append(coordinate)
        createPolygonFromTheDrawablePoints()
        addPinInMap()
    }
}
