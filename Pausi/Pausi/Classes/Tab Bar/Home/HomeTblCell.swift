//
//  HomeTblCell.swift
//  Pausi
//
//  Created by mac on 13/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

protocol HomeTblCellDelegate {
    func actionShowPopUp(index: IndexPath);
}

class HomeTblCell: UITableViewCell {

    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblId: UILabel!
    
    var delegate: HomeTblCellDelegate!
    var index: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionOptions(_ sender: Any) {
        delegate.actionShowPopUp(index: index)
    }
    
}
