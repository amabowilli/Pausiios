//
//  HomeCvCell.swift
//  Pausi
//
//  Created by mac on 31/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class HomeCvCell: UICollectionViewCell {
    
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPrice1: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnMoreOption: Button!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblBath: UILabel!
    @IBOutlet weak var lblBeds: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBAction func actionFav(_ sender: Any) {
    }
    
    @IBAction func actionShare(_ sender: Any) {
    }
    
}
