//
//  Filter.swift
//  Pausi
//
//  Created by mac on 27/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import DropDown

protocol FilterDelegate {
    func applyFilter(property_type: String, sale_type: String, min_price: String, max_price: String, min_bed: String, max_bed: String, min_bath: String, max_bath: String, status_by: String, date_by: String, sort_by: String, amenities: String)
    func resetFilter()
}


class Filter: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var vwSale: UIView!
    @IBOutlet weak var btnSale: UIButton!
    @IBOutlet weak var vwRent: UIView!
    @IBOutlet weak var btnRent: UIButton!
    @IBOutlet weak var lblPropertyType: UILabel!
    @IBOutlet weak var lblMinBeds: UILabel!
    @IBOutlet weak var lblMaxBeds: UILabel!
    @IBOutlet weak var lblMinBath: UILabel!
    @IBOutlet weak var lblMaxBath: UILabel!
    @IBOutlet weak var lblMinPrice: UILabel!
    @IBOutlet weak var lblMaxPrice: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSortBy: UILabel!
    @IBOutlet weak var heightCharacterstics: NSLayoutConstraint!
    @IBOutlet weak var vwCharacterstics: UIView!
    @IBOutlet weak var btnKitchen: UIButton!
    @IBOutlet weak var btnDining: UIButton!
    @IBOutlet weak var btnParking: UIButton!
    @IBOutlet weak var btnGarage: UIButton!
    @IBOutlet weak var btnStudy: UIButton!
    
    //MARK: VARIABLES
    var isSaleActive = true
    var dropDown = DropDown()
    var delegate: FilterDelegate?
    var saleType = "Sale"
    var propertyType = "All Property Types"
    var minPrice = "0"
    var maxPrice = "999999999"
    var minBed = "0"
    var maxBed = "999"
    var minBath = "0"
    var maxBath = "999"
    var status = "FOR SALE"
    var date = "Any"
    var sortBy = "LowToHigh"

    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        addAminities()
        filFillds()
        heightCharacterstics.constant = 0.0
        vwCharacterstics.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionUpload(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionForSale(_ sender: Any) {
        isSaleActive = changeSaleType("Sale")
    }
    
    @IBAction func actionForRent(_ sender: Any) {
        isSaleActive = changeSaleType("Rent")
    }
    
    @IBAction func actionPropertyType(_ sender: Any) {
        createDropDown(arrPropertyType, lblPropertyType)
    }
    
    var arrEminities = NSMutableArray()
    var isKitchen = false
    @IBAction func actionKitchen(_ sender: Any) {
        isKitchen = !isKitchen
        btnKitchen.setImage(getSelectImg(isKitchen, "Kitchen"), for: .normal)
    }
    
    var isDining = false
    @IBAction func actionDining(_ sender: Any) {
        isDining = !isDining
        btnDining.setImage(getSelectImg(isDining, "Dining"), for: .normal)
    }
    
    var isParking = false
    @IBAction func actionParking(_ sender: Any) {
        isParking = !isParking
        btnParking.setImage(getSelectImg(isParking, "Parking"), for: .normal)
    }
    
    var isGarage = false
    @IBAction func actionGarage(_ sender: Any) {
        isGarage = !isGarage
        btnGarage.setImage(getSelectImg(isGarage, "External Garage"), for: .normal)
    }
    
    var isStudy = false
    @IBAction func actionStudy(_ sender: Any) {
        isStudy = !isStudy
        btnStudy.setImage(getSelectImg(isStudy, "Study"), for: .normal)
    }
    
    @IBAction func actionMinBeds(_ sender: Any) {
        createDropDown(arrBedsMin, lblMinBeds)
    }
    
    @IBAction func actionMaxBeds(_ sender: Any) {
        createDropDown(arrBedsMax, lblMaxBeds)
    }
    
    @IBAction func actionMinBath(_ sender: Any) {
        createDropDown(arrBathsMin, lblMinBath)
    }
    
    @IBAction func actionMaxBath(_ sender: Any) {
        createDropDown(arrBathsMax, lblMaxBath)
    }
    
    @IBAction func actionMinPrice(_ sender: Any) {
        if isSaleActive {
            createDropDown(arrPriceSaleMin, lblMinPrice)
        } else {
            createDropDown(arrPriceRentMin, lblMinPrice)
        }
    }
    
    @IBAction func actionMaxPrice(_ sender: Any) {
        if isSaleActive {
            createDropDown(arrPriceSaleMax, lblMaxPrice)
        } else {
            createDropDown(arrPriceRentMax, lblMaxPrice)
        }
    }
    
    @IBAction func actionStatus(_ sender: Any) {
        if isSaleActive {
            createDropDown(arrSaleStatus, lblStatus)
        } else {
            createDropDown(arrRentStatus, lblStatus)
        }
    }
    
    @IBAction func actionDate(_ sender: Any) {
        createDropDown(arrDate, lblDate)
    }
    
    @IBAction func actionSortBy(_ sender: Any) {
        createDropDown(arrSort, lblSortBy)
    }
    
    @IBAction func actionApplyFilter(_ sender: Any) {
        delegate?.applyFilter(property_type: propertyType, sale_type: saleType, min_price: minPrice, max_price: maxPrice, min_bed: minBed, max_bed: maxBed, min_bath: minBath, max_bath: maxBath, status_by: status, date_by: date, sort_by: sortBy, amenities: arrEminities.componentsJoined(by: ","))
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionResetFilter(_ sender: Any) {
        delegate?.resetFilter()
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: FUNCTIONS
    func addAminities() {
        isKitchen = true
        btnKitchen.setImage(getSelectImg(isKitchen, "Kitchen"), for: .normal)
        isDining = true
        btnDining.setImage(getSelectImg(isDining, "Dining"), for: .normal)
        isParking = true
        btnParking.setImage(getSelectImg(isParking, "Parking"), for: .normal)
        isGarage = true
        btnGarage.setImage(getSelectImg(isGarage, "External Garage"), for: .normal)
        isStudy = true
        btnStudy.setImage(getSelectImg(isStudy, "Study"), for: .normal)
    }
    
    
    func getSelectImg(_ isBool: Bool, _ title: String) -> UIImage {
        if isBool {
            self.arrEminities.add(title)
            return UIImage(named: "check")!
        } else {
            self.arrEminities.remove(title)
            return UIImage(named: "uncheck")!
        }
    }
    
    func changeSaleType(_ type: String) -> Bool {
        minPrice = "0"
        maxPrice = "999999999"
        if type == "Sale" {
            saleType = "Sale"
            vwSale.backgroundColor = PredefinedConstants.appColor()
            vwRent.backgroundColor = UIColor.lightGray
            btnSale.setTitleColor(UIColor.white, for: .normal)
            btnRent.setTitleColor(UIColor.black, for: .normal)
            lblMinPrice.text = "0M"
            lblMaxPrice.text = "120M+"
            lblStatus.text = "For Sale"
            status = "FOR SALE"
            return true
        } else {
            saleType = "Rent"
            vwRent.backgroundColor = PredefinedConstants.appColor()
            vwSale.backgroundColor = UIColor.lightGray
            btnRent.setTitleColor(UIColor.white, for: .normal)
            btnSale.setTitleColor(UIColor.black, for: .normal)
            lblMinPrice.text = "0K"
            lblMaxPrice.text = "1M+"
            lblStatus.text = "For Rent"
            status = "FOR RENT"
            return false
        }
    }
    
    func createDropDown(_ arr: NSArray, _ lbl: UILabel) {
        dropDown.dataSource.removeAll()
        for i in 0..<arr.count {
            let dict = arr.object(at: i) as! NSDictionary
            dropDown.dataSource.append(string(dict, "key"))
        }
        dropDown.anchorView = lbl
        dropDown.selectionAction = { [unowned self] (index, item) in
            lbl.text = item
            let dict = arr.object(at: index) as! NSDictionary
            switch lbl {
            case self.lblPropertyType:
                self.propertyType = string(dict, "value")
                if item == "Apartment" {
                    self.heightCharacterstics.constant = 120.0
                    self.vwCharacterstics.isHidden = false
                } else {
                    self.heightCharacterstics.constant = 0.0
                    self.vwCharacterstics.isHidden = true
                }
            case self.lblMinBeds:
                self.minBed = string(dict, "value")
            case self.lblMaxBeds:
                self.maxBed = string(dict, "value")
            case self.lblMinBath:
                self.minBath = string(dict, "value")
            case self.lblMaxBath:
                self.maxBath = string(dict, "value")
            case self.lblMinPrice:
                self.minPrice = string(dict, "value")
            case self.lblMaxPrice:
                self.maxPrice = string(dict, "value")
            case self.lblStatus:
                self.status = string(dict, "value")
            case self.lblDate:
                self.date = string(dict, "value")
            case self.lblSortBy:
                self.sortBy = string(dict, "value")
            default:
                print("value-\(string(dict, "value"))-")
            }
        }
        dropDown.show()
    }
    
    func getKeyByValue(_ strValue: String, _ arr: NSArray) -> String {
        for i in 0..<arr.count {
            let dict = arr.object(at: i) as! NSDictionary
            if strValue == string(dict, "value") {
                return string(dict, "key")
            }
        }
        return ""
    }
    
    func filFillds() {
        
        if saleType == "Rent" {
            isSaleActive = changeSaleType("Rent")
            lblStatus.text = "For Sale"
            status = "FOR RENT"
            
            if minPrice == "" {
                lblMinPrice.text = "0K"
            } else {
                lblMinPrice.text = getKeyByValue(minPrice, arrPriceRentMin)
            }
            
            if maxPrice == "" {
                lblMaxPrice.text = "1M+"
            } else {
                lblMaxPrice.text = getKeyByValue(maxPrice, arrPriceRentMax)
            }
            
            if status == "" {
                lblStatus.text = "For Rent"
            } else {
                lblStatus.text = getKeyByValue(status, arrRentStatus)
            }
            
        } else {
            
            isSaleActive = changeSaleType("Sale")
            if minPrice == "" {
                lblMinPrice.text = "0M"
            } else {
                lblMinPrice.text = getKeyByValue(minPrice, arrPriceSaleMin)
            }
            
            if maxPrice == "" {
                lblMaxPrice.text = "120M+"
            } else {
                lblMaxPrice.text = getKeyByValue(maxPrice, arrPriceSaleMax)
            }
            
            if status == "" {
                lblStatus.text = "For Sale"
            } else {
                lblStatus.text = getKeyByValue(status, arrSaleStatus)
            }
        }
        
        if propertyType == "" {
            lblPropertyType.text = "All Propperty Types"
        } else {
            lblPropertyType.text = getKeyByValue(propertyType, arrPropertyType)
        }
        
        if minBed == "" {
            lblMinBeds.text = "0"
        } else {
            lblMinBeds.text = getKeyByValue(minBed, arrBedsMin)
        }
        
        if maxBed == "" {
            lblMaxBeds.text = "6+"
        } else {
            lblMaxBeds.text = getKeyByValue(minBed, arrBedsMax)
        }
        
        if minBath == "" {
            lblMinBath.text = "0"
        } else {
            lblMinBath.text = getKeyByValue(minBath, arrBathsMin)
        }
        
        if maxBath == "" {
            lblMaxBath.text = "5+"
        } else {
            lblMaxBath.text = getKeyByValue(maxBath, arrBathsMax)
        }
        
        if date == "" {
            lblDate.text = "Over 1 month"
        } else {
            lblDate.text = getKeyByValue(date, arrDate)
        }
        
        if sortBy == "" {
            lblSortBy.text = "Price (Low to High)"
        } else {
            lblSortBy.text = getKeyByValue(sortBy, arrSort)
        }
        
    }
    
    
    var arrPropertyType: NSArray = [["key": "All Property Types", "value": "All Property Types"], ["key": "Family Homes", "value": "Family Homes"], ["key": "Apartment", "value": "Apartment"], ["key": "Plots and Land", "value": "Plots and Land"], ["key": "Guest House", "value": "Guest House"], ["key": "Commercial", "value": "Commercial"]]
    var arrBedsMin: NSArray = [["key": "0", "value": "0"], ["key": "1", "value": "1"], ["key": "2", "value": "2"], ["key": "3", "value": "3"], ["key": "4", "value": "4"], ["key": "5", "value": "5"], ["key": "6", "value": "6"]]
    var arrBedsMax: NSArray = [["key": "0", "value": "0"], ["key": "1", "value": "1"], ["key": "2", "value": "2"], ["key": "3", "value": "3"], ["key": "4", "value": "4"], ["key": "5", "value": "5"], ["key": "6", "value": "6"], ["key": "6+", "value": "999"]]
    var arrBathsMin: NSArray = [["key": "0", "value": "0"], ["key": "1", "value": "1"], ["key": "2", "value": "2"], ["key": "3", "value": "3"], ["key": "4", "value": "4"], ["key": "5", "value": "5"]]
    var arrBathsMax: NSArray = [["key": "0", "value": "0"], ["key": "1", "value": "1"], ["key": "2", "value": "2"], ["key": "3", "value": "3"], ["key": "4", "value": "4"], ["key": "5", "value": "5"], ["key": "5+", "value": "999"]]
    var arrPriceSaleMin: NSArray = [["key": "0M", "value": "0"], ["key": "5M", "value": "5000000"], ["key": "10M", "value": "10000000"], ["key": "25M", "value": "25000000"], ["key": "100M", "value": "100000000"]]
    var arrPriceSaleMax: NSArray = [["key": "5M", "value": "5000000"], ["key": "10M", "value": "10000000"], ["key": "25M", "value": "25000000"], ["key": "100M", "value": "100000000"], ["key": "120M", "value": "120000000"], ["key": "120M+", "value": "999999999"]]
    var arrPriceRentMin: NSArray = [["key": "0K", "value": "0"], ["key": "10K", "value": "10000"], ["key": "50K", "value": "50000"], ["key": "100K", "value": "100000"], ["key": "1M", "value": "1000000"]]
    var arrPriceRentMax: NSArray = [["key": "0K", "value": "0"], ["key": "10K", "value": "10000"], ["key": "50K", "value": "50000"], ["key": "100K", "value": "100000"], ["key": "1M", "value": "1000000"], ["key": "1M+", "value": "999999999"]]
    var arrSaleStatus: NSArray = [["key": "For Sale", "value": "FOR SALE"], ["key": "Open House", "value": "OPEN HOUSE"], ["key": "Contract", "value": "CONTRACT"], ["key": "Sold", "value": "SOLD"]]
    var arrRentStatus: NSArray = [["key": "For Rent", "value": "FOR RENT"], ["key": "Open House", "value": "OPEN HOUSE"], ["key": "Pending", "value": "PENDING"], ["key": "Not Available", "value": "NOT AVAILABLE"]]
    var arrDate: NSArray = [["key": "Any", "value": "Any"], ["key": "Today", "value": "Today"], ["key": "In the past week", "value": "Week"], ["key": "In the past month", "value": "CurrentMonth"], ["key": "In the past 3 months", "value": "CurrentThreeMonth"], ["key": "In the past 6 months", "value": "CurrentSixMonth"], ["key": "Over 1 month", "value": "OverMonth"], ["key": "Over 3 months", "value": "OverThreeMonth"], ["key": "Over 6 months", "value": "OverSixMonth"]]
    var arrSort: NSArray = [["key": "Price (Low to High)", "value": "LowToHigh"], ["key": "Price (High to Low)", "value": "HighToLow"], ["key": "A to Z", "value": "ASC"], ["key": "Z to A", "value": "DESC"]]
    
}//Class End
