//
//  TabBarScreen.swift
//  Pausi
//
//  Created by mac on 11/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class TabBarScreen: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 2
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: TABBARCONTROLLER DELEGATE
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let navVC = viewController as? UINavigationController, kAppDelegate.navController != nil {
            let arr = navVC.viewControllers
            for vc in arr {
                if vc is AgentProfile {
                    let vcc = vc as! AgentProfile
                    kAppDelegate.navController?.popToViewController(vcc, animated: true)
                    return false
                } else if vc is Profile {
                    let vcc = vc as! Profile
                    kAppDelegate.navController?.popToViewController(vcc, animated: true)
                    return false
                }
            }
        }
        kAppDelegate.navController = nil
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let vc = viewController as? UINavigationController {
            vc.popToRootViewController(animated: false)
        }
    }
}
