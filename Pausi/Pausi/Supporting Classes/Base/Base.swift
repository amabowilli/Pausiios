//
//  Base.swift
//  Solviepro Hire
//
//  Created by mac on 28/05/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class Base: NSObject {
}

let appName = "PAUSI"

let appID = "1450156860"

let inviteText = "Welcome to Pausi\nYou can download app from\n\niTune Store:- https://itunes.apple.com/us/app/pausi/id1450156860?ls=1&mt=8\n\nPlay Store:- https://play.google.com/store/apps/details?id=com.t.pausi"


let kAppDelegate = UIApplication.shared.delegate as! AppDelegate

//let base_url = "http://technorizen.com/WORKSPACE1/pausi/webservice/"
let base_url = "http://35.180.157.237/pausi/webservice/"

let api_signup = "\(base_url)signup"

let api_login = "\(base_url)login"

let api_forgot_password = "\(base_url)forgot_password"

let api_user_update = "\(base_url)user_update"

let api_change_password = "\(base_url)change_password"

let api_update_user_image = "\(base_url)update_user_image"

let api_all_listing_list = "\(base_url)all_listing_list"

let api_listing_details = "\(base_url)listing_details"

let api_all_wallpaper_list = "\(base_url)all_wallpaper_list"

let api_set_wallpaper = "\(base_url)set_wallpaper"

let api_get_profile = "\(base_url)get_profile"

let api_add_to_favourite = "\(base_url)add_to_favourite"

let api_get_favourite_property_lists = "\(base_url)get_favourite_property_lists"

let api_all_services = "\(base_url)all_services"

let api_social_login = "\(base_url)social_login"

let api_update_zipcode = "\(base_url)update_zipcode"

let api_all_slider_image_list = "\(base_url)all_slider_image_list"

let api_get_chat = "\(base_url)get_chat"

let api_chat_message = "\(base_url)chat_message"

let api_insert_chat = "\(base_url)insert_chat"

let api_get_conversation = "\(base_url)get_conversation"

let api_news_lists = "\(base_url)news_lists"

let api_add_new_property = "\(base_url)add_new_property"

let api_add_recently_view_property = "\(base_url)add_recently_view_property"

let api_get_recently_viewed_property_lists = "\(base_url)get_recently_viewed_property_lists"

let api_my_listing_list = "\(base_url)my_listing_list"

let api_delete_property = "\(base_url)delete_property"

let api_delete_property_image = "\(base_url)delete_property_image"

let api_update_property = "\(base_url)update_property"

let api_add_remove_notify = "\(base_url)add_remove_notify"

let api_get_property_notification = "\(base_url)get_property_notification"

let api_update_property_status = "\(base_url)update_property_status"

let api_clear_conversation = "\(base_url)clear_conversation"

let api_update_online_offline_status = "\(base_url)update_online_offline_status"

let api_get_all_user_list = "\(base_url)get_all_user_list"

let api_get_property_sold_history_list = "\(base_url)get_property_sold_history_list"






/********************************AlertMessage*******************************/

class AlertMsg: NSObject {
    
    
    static let blankEmail = "Please enter email address.".getLoclized()
    static let invalidEmail = "Please enter valid email address.".getLoclized()
    static let blankPassword = "Please enter password.".getLoclized()
    static let blankConfPass = "Please enter confirm password.".getLoclized()
    static let passMisMatch = "Password and confirm password does not match.".getLoclized()
    static let blankFirstName = "Please enter first name.".getLoclized()
    static let blankLastName = "Please enter last name.".getLoclized()
    static let blankNumber = "Please enter mobile number.".getLoclized()
    static let acceptTerms = "Please agree terms and conditions.".getLoclized()
    static let blankCrntPass = "Please enter your current password.".getLoclized()
    static let blankNewPass = "Please enter your new password.".getLoclized()
    static let passMisMatchNew = "New password and confirm password does not match.".getLoclized()
    static let blankBroker = "Please enter broker type.".getLoclized()
    static let selectPhoto = "Please upload photo.".getLoclized()
    static let selectID = "Please upload your identity card.".getLoclized()
    static let blankZip = "Please enter zip code.".getLoclized()
    
    static let blankPropertyTitle = "Please enter property title.".getLoclized()
    static let selectPropertyFor = "Please select property for sell or rent.".getLoclized()
    static let selectAvailableFor = "Please select property status.".getLoclized()
    static let blankAddress = "Please enter address.".getLoclized()
    static let blankLendMark = "Please search for street/area/landmark.".getLoclized()
    static let blankPropertyType = "Please select property type.".getLoclized()
    static let blankBed = "Please enter number of beds.".getLoclized()
    static let blankBath = "Please enter numebr of baths.".getLoclized()
    static let blsnkSize = "Please enter property size.".getLoclized()
    static let blankBuiltIn = "Please select built in year.".getLoclized()
    static let blankPrice = "Please enter price.".getLoclized()
    static let blankDescription = "Please enter description.".getLoclized()
    static let selectImage = "Please attache atleast one image.".getLoclized()
    static let submitSuccess = "Submitted successfully".getLoclized()
    static let updateSuccess = "Update successfully".getLoclized()
    static let updateProfile = "Your profile is incompleted!!! You have to upload your profile.".getLoclized()
    static let cantChat = "You can't chat for your own property.".getLoclized()
    static let blankSoldPrice = "Please enter sold price.".getLoclized()

}

