//
//  KSLocation.swift
//  Pausi
//
//  Created by mac on 14/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import CoreLocation

class KSLocation: NSObject, CLLocationManagerDelegate {
    
    var currentLocation: CLLocation? = nil
    let locationManager = CLLocationManager()
    
    func startLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.requestLocation()
        
    }
    
    func stopLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    /*func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }*/
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Http.alert(appName, error.localizedDescription)
    }
    

}
